"""
Module for different popups.

A popup is a view that will be drawn in the
foreground of all normal views when it is shown.

Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""
import time
from abc import ABC
from threading import Thread
from typing import Optional, List, Callable

from legit.config import Config, Field, Section
from legit.context import Context
from legit.handler import KeyCommandHandler, key_command
from legit.ui.base import Drawable

confirm_command_cb = Callable[[], None]
enter_command_cb = Callable[[str], None]
OnCancel = Callable[[], None]


class Popup(Drawable, KeyCommandHandler, ABC):
    """
    Base class for popups doing generic initialization.

    The size of the popup is adjusted to the content to be rendered,
    but at most rows of viewport / 2. Popups by default are immutable,
    therefore neither the title nor the text can be changed.

    :ivar _has_changed: Flag whether the internal state has changed.
                        This is mainly used to update the size of
                        the popup.
    :ivar _on_cancel:   Callback that is called, when the popup
                        is cancelled
    """

    def __init__(self, ctx: Context,
                 text: Optional[str] = None, title: Optional[str] = None,
                 on_cancel: Optional[OnCancel] = None):
        """
        Init the popup windows, register key commands and init generic data.

        :param ctx:       The context of the application
        :param text:      The content of the popup
        :param title:     The title of the popup
        :param on_cancel: The callback that is called when
                          the popup is cancelled
        """
        Drawable.__init__(self, ctx, text, title)
        KeyCommandHandler.__init__(self)

        self._has_changed: bool = True
        self._on_cancel: Optional[OnCancel] = on_cancel

    def draw(self) -> None:
        """ Adjust the popup to content if it has changed and draw it. """
        if self._has_changed:
            self.scale_to_viewport()
            self._has_changed = False
        super().draw()

    def scale_to_viewport(self) -> None:
        """
        Scale the popup to the current viewport size.

        Scaling takes the current viewport size into account
        but also adjusts the popup size to fit the content.

        The size is adjusted like the following:
            1. Width: The minimum of ...
                1.  half width of the screen
                2.  the length of the longest line + a padding,
                    so that small content is displayed nicely
            2. Height: The minimum of ... + the border size of 2
                1.  half of the height of the screen
                2.  maximum number of content rows
        """
        max_rows, max_cols = self._ctx.root_window.getmaxyx()
        border_size: int = 2
        max_line_len: int = 0
        max_title_len: int = 0
        title_rows: int = 0
        width_pad: int = 20

        if self._lines:
            max_line_len = len(max(self._lines, key=len))

        if self._title is not None:
            max_title_len = len(self._title)
            title_rows = 2 if self._lines else 1

        content_rows: int = len(self._lines)
        max_content_rows: int = content_rows + title_rows
        rows: int = min(max_rows // 2, max_content_rows) + border_size
        cols = min(max_cols // 2,
                   max(max_line_len, max_title_len) + width_pad) + border_size

        y: int = (max_rows - rows) // 3
        x: int = (max_cols - cols) // 2
        self._rows: int = rows - border_size
        self._cols: int = cols - border_size
        self._border_box = self._ctx.root_window.derwin(rows, cols, y, x)
        self._window = self._border_box.derwin(self._rows, self._cols, 1, 1)

    def show(self) -> None:
        """ Make the popup visible in the viewport. """
        self._ctx.env.popup = self

    def hide(self) -> None:
        """ Hide the popup. """
        self._ctx.env.popup = None

    def cancel(self) -> None:
        """ Hide the loading popup and call on-cancel callback. """
        self.hide()
        if self._on_cancel:
            self._on_cancel()

    def _register_key_commands(self) -> None:
        c: Config = self._ctx.config
        cancel_key: int = c.get_key(Section.GLOBAL_KEYS, Field.CANCEL)
        super().register_key_command(cancel_key, self.cancel, 'cancel')


class TextPopup(Popup):
    """
    A popup showing static text data.

    The content text is scrollable if it exceeds the popup's size.
    """

    def scroll_up(self) -> None:
        """ Scroll the whole view content up. """
        if self._y_start > 0:
            self._y_start -= 1

    def scroll_down(self) -> None:
        """ Scroll the whole view content down. """
        if not self._lines:
            return

        rows, cols = self._window.getmaxyx()
        line_count: int = len(self._lines)
        title_rows = 2

        if line_count <= rows - title_rows:
            return

        if self._y_start < line_count - 1:
            self._y_start += 1

    def _register_key_commands(self) -> None:
        super()._register_key_commands()

        c: Config = self._ctx.config
        up_key: int = c.get_key(Section.GLOBAL_KEYS, Field.SCROLL_UP)
        down_key: int = c.get_key(Section.GLOBAL_KEYS, Field.SCROLL_DOWN)
        super().register_key_command(up_key, self.scroll_up)
        super().register_key_command(down_key, self.scroll_down)


class ConfirmPopup(TextPopup):
    """
    A popup for confirming an action.

    :ivar _on_yes_cb: Callback, executed when the popup is confirmed
    :ivar _on_no_cb:  Callback, executed when the popup is denied
    """

    def __init__(self, ctx: Context, title: str,
                 on_yes: Optional[confirm_command_cb] = None,
                 on_no: Optional[confirm_command_cb] = None):
        """
        :param on_yes: A callback to be called when 'yes' is triggered
        :param on_no:  A callback to be called when 'no' is triggered
        """
        c: Config = ctx.config
        yes_key: int = c.get_key(Section.GLOBAL_KEYS, Field.YES)
        no_key: int = c.get_key(Section.GLOBAL_KEYS, Field.NO)

        super().__init__(ctx, None, title)

        self._on_yes_cb: Optional[confirm_command_cb] = on_yes
        self._on_no_cb: Optional[confirm_command_cb] = on_no
        self.register_key_command(yes_key, self.on_yes, 'Yes')
        self.register_key_command(no_key, self.on_no, 'No')
        self.text = self.generate_summary()

    def on_yes(self) -> None:
        """ Trigger on yes callback and close popup. """
        self.hide()
        if self._on_yes_cb:
            self._on_yes_cb()

    def on_no(self) -> None:
        """ Trigger on no callback and close popup. """
        self.hide()
        if self._on_no_cb:
            self._on_no_cb()


class InputPopup(Popup):
    """
    A popup showing a simple input field to write text into.

    The popup can be used for plain text as well as for entering

    :ivar _on_enter_cb: Callback, executed when `enter` is pressed
    :ivar _plain_text:  The input as plain text
    :ivar _password:    A flag whether the input should be masked
    """

    def __init__(self, ctx: Context, title: str,
                 on_enter: enter_command_cb, password: bool = False):
        """
        :param on_enter: A callback function when the input is submitted
        :param password: A flag indicating whether the input text should
                         be masked
        """
        super().__init__(ctx, '', title)

        self._on_enter_cb: enter_command_cb = on_enter
        self._plain_text: str = ''
        self._password: bool = password

    def draw(self) -> None:
        """
        Draw the input popup.

        If password is True, the input will be represented by `*`.
        """
        self._lines = [self._plain_text if not self._password else '*' * len(
            self._plain_text)]
        super().draw()

    def handle_command(self, key: int) -> bool:
        """
        Write a char into the textbox or handle navigation.

        :param key: The pressed key
        :return:    True
        """
        if not super().handle_command(key):
            self._plain_text += chr(key)

        return True

    def _on_enter(self) -> None:
        """ Call on-enter with the current text and hide the popup. """
        self.hide()
        self._on_enter_cb(self._plain_text)

    def _remove_char(self) -> None:
        """ Remove the last character of the text. """
        if len(self._plain_text) > 0:
            self._plain_text = self._plain_text[:-1]

    def _register_key_commands(self) -> None:
        super()._register_key_commands()

        c: Config = self._ctx.config
        enter_key: int = c.get_key(Section.GLOBAL_KEYS, Field.ENTER)
        backspace_key: int = c.get_key(Section.GLOBAL_KEYS, Field.BACKSPACE)
        self.register_key_command(enter_key, self._on_enter)
        self.register_key_command(backspace_key, self._remove_char)


class LoadingPopup(Popup):
    """
    A popup showing a loading animation.

    :ivar _thread: The thread drawing the loading animation
    """

    FRAMES: List[str] = ["⠋", "⠙", "⠹", "⠸", "⠼", "⠴", "⠦", "⠧", "⠇", "⠏"]

    def __init__(self, ctx: Context, title: str,
                 on_cancel: Optional[OnCancel]):
        """
        Init the loading popup.

        :param ctx:       The context of the application
        :param title:     The title of the input box
        :param on_cancel: A callback function when the
                          input is submitted
        """
        super().__init__(ctx, None, title, on_cancel)

        self._thread: Thread = Thread(target=self._run)

    def show(self) -> None:
        """ Show popup and start animation thread. """
        super().show()
        self._thread.start()

    def hide(self) -> None:
        """ Hide popup and stop animation thread. """
        super().hide()
        setattr(self._thread, "running", False)

    def _run(self) -> None:
        """ Draw a loading animation as long as `running` is set to False. """

        self._title = f' {self.FRAMES[0]} {self._title}'

        index: int = 0
        length: int = len(self.FRAMES)

        while getattr(self._thread, "running", True):
            self._title = f' {self.FRAMES[index]} {self._title[3:]}'
            self._window.erase()
            self.draw()
            self._window.refresh()
            index = (index + 1) % length
            time.sleep(0.1)


class CommandPopup(TextPopup):
    """
    A popup showing available commands for an action.

    In contrast to other popups only the title can be set from
    outside, but the content is created dynamically showing a
    summary of all registered key commands, that have been
    registered with a summary.
    """

    def __init__(self, ctx: Context, title: Optional[str],
                 on_cancel: Optional[OnCancel] = None):
        """
        :param ctx:       The context of the application
        :param title:     The title of the popup
        :param on_cancel: A callback called when the popup is canceled
        """
        super().__init__(ctx, None, title, on_cancel)

    def draw(self) -> None:
        if self._has_changed:
            self.text = self.generate_summary()
            self.scale_to_viewport()
            self._has_changed = False
        super().draw()

    def register_key_command(self, key: int, command: key_command,
                             summary: Optional[str] = None,
                             description: Optional[str] = None) -> None:
        """ Register a key command and wrap it to hide popup on execution. """

        def hide_on_command() -> None:
            """ Hide popup and execute key command. """
            self.hide()
            command()

        super().register_key_command(key, hide_on_command, summary,
                                     description)
