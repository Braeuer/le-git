"""
A module containing a base class which offers generic draw functionality.

Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""

import curses
from abc import ABC
from typing import Optional, List

from legit import scheme
from legit.context import Context
from legit.ui import render
from legit.ui.render import ColorGroup


class Drawable(ABC):
    """
    A base class for all drawable objects.

    A drawable consists of a border box, that renders the border, and a
    window, that renders the content (title/text). If `_draw_border`
    is False, the space for the border is 'allocated' but nothing is
    rendered. If a drawable has no title, the content fills out the space
    of the title.

    A drawable with border and title looks like the following:

    * ---------------------------------------------------------------- *
    |                              Title                               |
    |------------------------------------------------------------------|
    |                                                                  |
    |                                                                  |
    |                                                                  |
    |                             Content                              |
    |                                                                  |
    |                                                                  |
    |                                                                  |
    * ---------------------------------------------------------------- *

    :ivar _ctx:         The current context of the application
                        holding global objects/data
    :ivar _cg:          The color group used for coloring different parts
                        of each line by RegEx
    :ivar _border_box:  The border box rendering the border
    :ivar _window:      The window rendering the view content
                        (title/text)
    :ivar _draw_border: Whether the border should be rendered
    :ivar _title:       The title of the view
    :ivar _lines:       The lines of the view
    :ivar _title_rows:  The number of rows spanned by the title
    :ivar _rows:        The number of rows spanned by the content window
    :ivar _cols:        The number of columns spanned by the content window
    :ivar _y:           The vertical position relatively to the top of the
                        window
    :ivar _x:           The horizontal position relatively to the left of
                        the window
    :ivar _y_start:     The index of the first line to start rendering
                        the view
    :ivar _x_start:     The index of the first character of a line to
                        be rendered
    :ivar _selected:    The currently selected item to be rendered
                        highlighted. If negative, no line will be
                        highlighted.
    """

    def __init__(self, ctx: Context, text: Optional[str] = None,
                 title: Optional[str] = None, draw_border: bool = True):
        """
        Initialize data needed for drawing (unavailable with default value).

        :param ctx:         The context of the application
        :param text:        The text to be rendered
        :param title:       The title to be rendered
        :param draw_border: Whether a border should be drawn
        """
        self._ctx: Context = ctx
        self._cg: Optional[ColorGroup] = None
        self._border_box: Optional[curses.window] = None
        self._window: curses.window = self._ctx.root_window
        self._draw_border: bool = draw_border
        self._title: Optional[str] = title
        self._lines: List[str] = []
        self._title_rows: int = 0
        self._rows: int = 0
        self._cols: int = 0
        self._y: int = 0
        self._x: int = 0
        self._y_start: int = 0
        self._x_start: int = 0
        self._selected: int = -1
        self.text = text

    @property
    def text(self) -> Optional[str]:
        """
        Get the currently displayed text.

        :return: The currently displayed text or None if there i none
        """
        return '\n'.join(self._lines)

    @text.setter
    def text(self, text: Optional[str]) -> None:
        """
        Set text to be rendered.

        **Note**: CRs and trailing whitespaces are removed from
                  the text

        :param text: The text to be rendered or None to clear
        """
        if text is None:
            self._lines = []
        else:
            self._lines = text.replace('\r', '').rstrip().split('\n') if (
                self.text is not None) else []

    @property
    def title(self) -> Optional[str]:
        """
        Get the currently rendered title.

        :return: The current title
        """
        return self._title

    @title.setter
    def title(self, title: Optional[str]) -> None:
        """
        Set a title to be rendered.

        :param title: The title to be rendered or None to clear th title
        """
        self._title = title

    def draw(self) -> None:
        """
        Draw the view with a border and a title if there is a title.

        The selected item will be drawn as highlighted.
        """
        window: curses.window = self._window
        rows: int = self._rows
        cols: int = self._cols
        y: int = self._y
        x: int = self._x
        color: int = scheme.get_color(scheme.Type.NORMAL)
        c_divider: Optional[int] = (scheme.get_color(scheme.Type.DIVIDER)
                                    if self._lines else None)

        if self._draw_border and self._border_box is not None:
            self._border_box.box()

        if self._title:
            title_rows: int = render.title(window, self._title, cols,
                                           color, c_divider)
            rows -= title_rows
            y += title_rows

        if self._lines:
            render.lines(window, self._lines, self._selected, color, self._cg,
                         rows, cols, y, x,
                         self._y_start, self._x_start)
