"""
A collection of base layouts that can be applied.

Layouts:
    1.   Layout       - Base layout which can be extended
    2.   TabbedLayout - Layout with a bottom TabView and multiple
                        layouts to switch between them

Author: Matthias Bräuer <matthias@brauer.dev>
License: MIT
"""
import curses
from abc import abstractmethod, ABC
from typing import List, Dict, Optional

from legit.config import Config, Section, Field
from legit.context import Context
from legit.handler import KeyCommandHandler
from legit.ui.views import base


class Layout(KeyCommandHandler, ABC):
    """
    Base class for layouts that provides generic functionality.

    A layout is a collection of views to be rendered.

    :ivar _ctx:   The current context of the application holding
                  global objects/data
    :ivar _views: The views of the layout
    """

    def __init__(self, ctx: Context):
        """
        Create the layout's widget set and call init callback of subclasses.

        :param ctx:  The current context of the application
        """
        self._ctx: Context = ctx
        self._views: Dict[str, base.View] = {}

        super().__init__()

    @abstractmethod
    def set_size(self, parent: curses.window, rows: int, cols: int, y: int,
                 x: int) -> None:
        """
        Set the size of all views within the layout.

        **Note**: The number of rows + y **must not** be greater
                  than the number of rows of the parent
        **Note**: The number of columns + x **must not** be greater
                  than the number of columns of the parent

        :param parent: The parent window
        :param rows:   The number of rows to be spanned (> 0)
        :param cols:   The number of columns to be spanned (> 0)
        :param y:      The vertical position relatively to
                       the top of the window (>= 0)
        :param x:      The horizontal position relatively to
                       the left of the window (>= 0)
        """

    def handle_command(self, key: int) -> bool:
        """
        Find a command for a key in the layout and all its views to execute it.

        The first command that was found will be executed.

        :param key: The key of the command to be executed
        :return:    True if a command was executed, else False
        """
        if super().handle_command(key):
            return True

        return any(v.handle_command(key) for _k, v in self._views.items())

    def reload(self) -> None:
        """ Reload all views. """
        for _k, v in self._views.items():
            v.reload()

    def draw(self) -> None:
        """ Draw all views. """
        for _k, v in self._views.items():
            v.draw()


class TabbedLayout(Layout):
    """
    A layout that let's one switch between different layouts.

    This layout shows a TabView at the bottom of its window.

    The tabbed layout looks like the following:

    * ---------------------------------------------------------------- *
    |                                                                  |
    |                                                                  |
    |                                                                  |
    |                                                                  |
    |                             Layouts                              |
    |                                                                  |
    |                                                                  |
    |                                                                  |
    |                                                                  |
    * ---------------------------------------------------------------- *
    * ---------------------------------------------------------------- *
    |                             TabView                              |
    * ---------------------------------------------------------------- *
    """

    def __init__(self, ctx: Context):
        super().__init__(ctx)

        self._layouts: List[Layout] = []
        self._index: int = 0
        self._tab_view: base.TabView = base.TabView(ctx, False)
        self._register_key_commands()

    @property
    def selected(self) -> Layout:
        """
        Get the selected layout.

        :return: The selected layout
        """
        return self._layouts[self._index]

    @property
    def index(self) -> int:
        """
        Get the index of the currently selected layout.

        :return: The index of the layout
        """
        return self._index

    def set_size(self, parent: curses.window, rows: int, cols: int, y: int,
                 x: int) -> None:
        tab_rows: int = 3
        rows -= tab_rows

        self._tab_view.set_relative_size(parent, tab_rows, cols, rows + y, x)

        for layout in self._layouts:
            layout.set_size(parent, rows, cols, y, x)

    def reload(self) -> None:
        """ Reload all layouts in this layout set. """
        for layout in self._layouts:
            layout.reload()

    def draw(self) -> None:
        """ Draw the layout at the current index. """
        self._index %= len(self._layouts)
        self._layouts[self._index].draw()

        if self._tab_view:
            self._tab_view.highlight_at(self._index)
            self._tab_view.draw()

    def handle_command(self, key: int) -> bool:
        """
        Find a command for a key in the layout set and the applied layout.

        The first command that was found will be executed.

        :param key: The key of the command to be executed
        :return:    True if a command was executed, else False
        """
        if not super().handle_command(key):
            return self.selected.handle_command(key)

        return False

    def apply_next(self) -> None:
        """ Switch to the next layout in the current layout set. """
        self._index += 1
        self.draw()

    def apply_prev(self) -> None:
        """ Switch to the previous layout in the current layout set. """
        self._index -= 1
        self.draw()

    def apply_at(self, index: int) -> None:
        """
        Switch to the layout at index.

        If the index is outside the range of the layouts list,
        it will be normalised with a modulo operation, so it
        matches a layout in the list.

        :param index: The index of the layout to switch to
        """
        self._index = index
        self.draw()

    def add_layout(self, icon: str, layout: Layout) -> None:
        """
        Add a layout to the tabbed layout.

        **Note**: The size of the layout will now be handled
                  by the tabbed layout.
        """
        self._layouts.append(layout)
        self._tab_view.add_icon(icon)

    def _register_key_commands(self) -> None:
        """ Register key commands for switching between layouts. """
        c: Config = self._ctx.config
        next_key: int = c.get_key(Section.GLOBAL_KEYS, Field.NEXT_LAYOUT)
        prev_key: int = c.get_key(Section.GLOBAL_KEYS, Field.PREV_LAYOUT)
        switch_1: int = c.get_key(Section.GLOBAL_KEYS, Field.SWITCH_LAYOUT_1)
        switch_2: int = c.get_key(Section.GLOBAL_KEYS, Field.SWITCH_LAYOUT_2)
        self.register_key_command(next_key, self.apply_next)
        self.register_key_command(prev_key, self.apply_prev)
        self.register_key_command(switch_1, lambda: self.apply_at(0))
        self.register_key_command(switch_2, lambda: self.apply_at(1))


class SplitLayout(Layout):
    """
    A split layout that has a top/bottom/left/right view with specific sizes.

    Each of the views is optional. If a view is not set, the space
    remains empty. The top and bottom view are both fixed to a height
    of 3 rows. The remaining space is given to the views on the left
    and right.

    The layout with all views set looks like the following:

    * ---------------------------------------------------------------- *
    |                               TOP                                |
    * ---------------------------------------------------------------- *
    * ----------------------------- * * ------------------------------ *
    |                               | |                                |
    |                               | |                                |
    |                               | |                                |
    |             LEFT              | |             RIGHT              |
    |                               | |                                |
    |                               | |                                |
    |                               | |                                |
    * ----------------------------- * * ------------------------------ *
    * ---------------------------------------------------------------- *
    |                              BOTTOM                              |
    * ---------------------------------------------------------------- *
    """
    LEFT: str = "LEFT"
    RIGHT: str = "RIGHT"
    TOP: str = "TOP"
    BOTTOM: str = "BOTTOM"

    def __init__(self, ctx: Context, top: Optional[base.View],
                 bottom: Optional[base.View],
                 left: Optional[base.View],
                 right: Optional[base.View]):
        """
        Add views.

        :param ctx:    The context of the application
        :param top:    The top view to be rendered
        :param bottom: The bottom view to be rendered
        :param left:   The left view to be rendered
        :param right:  The right view to be rendered
        """
        super().__init__(ctx)

        if top is not None:
            self._views[self.TOP] = top
        if bottom is not None:
            self._views[self.BOTTOM] = bottom
        if left is not None:
            self._views[self.LEFT] = left
        if right is not None:
            self._views[self.RIGHT] = right

    def set_size(self, parent: curses.window, rows: int, cols: int, y: int,
                 x: int) -> None:
        top_rows: int = 3
        bottom_rows: int = 3
        bottom_y: int = y + top_rows + rows
        rows -= (bottom_rows + top_rows)
        cols //= 2

        if self._views.get(self.TOP) is not None:
            self._views[self.TOP].set_relative_size(parent, top_rows,
                                                    cols, y, x)
        if self._views.get(self.BOTTOM) is not None:
            self._views[self.BOTTOM].set_relative_size(parent, bottom_rows,
                                                       cols, bottom_y, x)
        if self._views.get(self.LEFT) is not None:
            self._views[self.LEFT].set_relative_size(parent, rows, cols,
                                                     y + top_rows, x)
        if self._views.get(self.RIGHT) is not None:
            self._views[self.RIGHT].set_relative_size(parent, rows, cols,
                                                      y + top_rows, x + cols)
