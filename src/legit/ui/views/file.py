"""
A collection of views working on file nodes.

Views:
    1. TreeView - Shows a file tree
    2. InfoView - Shows information on a specific file node

Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""
import pathlib
from dataclasses import dataclass
from enum import Enum
from typing import List, Optional, Set, Dict

from legit import helper, git, scheme
from legit.config import Section, Field, Config
from legit.context import Context
from legit.ui.popup import CommandPopup, TextPopup
from legit.ui.render import (glue_regex, COMMIT_REGEX, HASH_REGEX,
                             AUTHOR_REGEX, DATE_REGEX, DATE_DEFAULT_REGEX,
                             ADDED_REGEX, REMOVED_REGEX, EMAIL_REGEX,
                             NAME_REGEX, SINGLE_NAME_REGEX, ColorGroupElement)
from legit.ui.views.base import SelectView, TextView


def get_pretty_path_name(path: pathlib.Path) -> str:
    """
    Get the name of a file/directory of path.

    For files just the name is returned, for
    directory a trailing '/' is added.

    :param path: The path to the file/directory
    :return: The name of the file or the name of
             the directory with a trailing '/'
    """
    return f'{path.name}/' if path.is_dir() else f'{path.name}'


class TreeView(SelectView['TreeView.Node']):
    """
    A view for listing all files and directories within a repository.

    All variables and actions are within the context of a Git repository.

    :ivar _staged_files:     The staged and yet pushed files
    :ivar _new_staged_files: The staged and not yet pushed files
    :ivar _unstaged_files:   The not yet staged files
    :ivar _untracked_files:  The not yet tracked files
    """

    @dataclass
    class Node(SelectView.Item):
        """
        A file/directory and its current state within the file tree.

        :ivar path:  The path to this file node
        :ivar icon:  An icon representing the type/state of the node
        :ivar state: The Git state of the node (e.g. staged, new, ...)
        """

        class State(Enum):
            """ An enumeration of different node states in Git context. """
            DEFAULT = ''
            STAGED = 'staged'
            NEW_STAGED = 'new'
            UNSTAGED = 'unstaged'
            UNTRACKED = 'untracked'

        path: pathlib.Path
        icon: str
        state: State = State.DEFAULT

        @classmethod
        def create(cls, children: Optional[List['TreeView.Node']],
                   level: int, path: pathlib.Path, icon: str,
                   state: 'TreeView.Node.State') -> 'TreeView.Node':
            """
            Create a new file tree node with default values.

            The node has the following default state:
                1.  Not selected
                2.  Not marked
                3.  Not expanded

            :param children: A list of child file nodes. If file node is not
                             a directory this field **must not** be used.
            :param level:    ahe level in the file tree (repository root = 0)
            :param path:     The path to this file node
            :param icon:     An icon representing the type/state of the node
            :param state:    The Git state of the node (e.g. staged, new, ...)
            """
            node: TreeView.Node = TreeView.Node('', False, False, level,
                                                children, False, path, icon,
                                                state)
            return node

        @property
        def display_name(self) -> str:
            """
            Get the display name based on the node's internal state.

            :return: The display name
            """
            margin: str = ' ' * 3 * self.level
            prefix: str = '* ' if self.marked else ''
            state: str = f' ({self.state.value})' if self.state.value else ''
            return f'{margin}{prefix}{self.icon} {self.path.name}{state}'

        @display_name.setter
        def display_name(self, display_name: str) -> None:
            """
            Do nothing as for file tree nodes the display name cannot be set.
            """

    def __init__(self, ctx: Context, draw_border: bool = True):
        super().__init__(ctx, None, None, draw_border)
        self._staged_files: Set[str] = set()
        self._new_staged_files: Set[str] = set()
        self._unstaged_files: Set[str] = set()
        self._untracked_files: Set[str] = set()

        def create_state_cg_element(
            status: TreeView.Node.State,
            scheme_type: scheme.Type
        ) -> ColorGroupElement:
            """
            Create a color group element for a specific state.

            :param status:      The status to be matched
            :param scheme_type: The color to paint the status if it matches
            :return:            The created color group element
            """
            return glue_regex([
                (r".* \(", scheme.get_color(scheme.Type.NORMAL)),
                (fr"{status.value}", scheme.get_color(scheme_type.value)),
                (r"\)", scheme.get_color(scheme.Type.NORMAL))
            ], scheme.get_color(scheme.Type.NORMAL))

        self._cg = [
            create_state_cg_element(TreeView.Node.State.UNSTAGED,
                                    scheme.Type.UNSTAGED),
            create_state_cg_element(TreeView.Node.State.STAGED,
                                    scheme.Type.STAGED),
            create_state_cg_element(TreeView.Node.State.NEW_STAGED,
                                    scheme.Type.NEW_STAGED),
            create_state_cg_element(TreeView.Node.State.UNTRACKED,
                                    scheme.Type.UNTRACKED)
        ]

    def draw(self) -> None:
        """ Reload view if there are no file nodes and draw it. """
        if not self._items:
            self.reload()
        super().draw()

    def reload(self) -> None:
        """
        (Re-)initialize all files/directories within a directory.

        **Note**: Triggers the on-selected callback
        """
        if self._ctx.git is None:
            return

        path: pathlib.Path = self._ctx.git.path
        self._init_changed_files()
        self._items = self._get_file_nodes(path, 0)
        self._sync_internal_state()
        self._trigger_on_selected_cb()

    def open(self) -> None:
        """ Open directory or open file in system editor. """
        item: Optional[TreeView.Node] = self.item

        if item is None:
            return

        if item.children is not None:
            if not item.expanded and not item.children:
                item.children = self._get_file_nodes(item.path,
                                                     item.level + 1)

            icon: str = self._ctx.config.get_directory_icon(True)
            item.icon = icon
            item.expanded = True
            self._sync_internal_state()
        else:
            helper.open_file_in_editor(item.path)
            self._ctx.root_window.redrawwin()

    def close(self) -> None:
        """ Close selected directory if it is expanded. """
        item: Optional[TreeView.Node] = self.item

        if item is not None and item.expanded:
            icon: str = self._ctx.config.get_directory_icon(False)

            item.expanded = False
            item.icon = icon
            self._sync_internal_state()

    def _get_file_nodes(self, directory: pathlib.Path,
                        level: int) -> List['TreeView.Node']:
        """
        Get all files and directories of a directory.

        :param directory: The directory to get all file nodes from
        :param level:     The directory level of the files.
                          0 project root,
                          1 within a directory in project root,
                          2 within directory of directory in project
                            root,
                          3 ...
        :return:          A list containing all file nodes, or an empty
                          list if directory is empty
        """
        icon: str
        files: List[TreeView.Node] = []
        state: TreeView.Node.State
        children: Optional[List[TreeView.Node]]

        for file_path in directory.iterdir():
            if file_path.is_dir():
                children = []
                state = TreeView.Node.State.DEFAULT
                icon = self._ctx.config.get_directory_icon(False)
            else:
                children = None
                state = self._get_node_state(file_path)
                file_ending: str = ''.join(file_path.suffixes)
                icon = self._ctx.config.get_file_icon(file_ending)

            files.append(
                TreeView.Node.create(children, level, file_path,
                                     icon, state))

        return files

    def _get_node_state(self,
                        file_path: pathlib.Path) -> 'TreeView.Node.State':
        """
        Get the Git status of a file node.

        Currently multiple Git states are merged

        :param file_path: The path of the file node
        :return:          The Git status of the file node
        """
        path: str = str(file_path)

        if path in self._unstaged_files:
            return TreeView.Node.State.UNSTAGED
        elif path in self._staged_files:
            return TreeView.Node.State.STAGED
        elif path in self._untracked_files:
            return TreeView.Node.State.UNTRACKED
        elif path in self._new_staged_files:
            return TreeView.Node.State.NEW_STAGED
        else:
            return TreeView.Node.State.DEFAULT

    def _init_changed_files(self) -> None:
        """
        Initialize different sets of changed files depending on the file state.

        Following states are supported:
            *   Staged
            *   Unstaged
            *   New and staged
            *   Untracked
        """
        if self._ctx.git is None:
            return

        path: pathlib.Path = self._ctx.git.path
        changed_files: List[str] = self._ctx.git.exec('status', None,
                                                      ['--porcelain'])

        self._staged_files = set()
        self._new_staged_files = set()
        self._unstaged_files = set()
        self._untracked_files = set()

        states: Dict[str, Set] = {
            'MM ': self._staged_files,
            'M  ': self._staged_files,
            ' M ': self._unstaged_files,
            'A  ': self._new_staged_files,
            'AM ': self._new_staged_files,
            '?? ': self._untracked_files
        }

        for file in changed_files:
            state: str = file[:3]
            file_name: str = f'{path}/{file[3:]}'
            # TODO: Display unknown states as '(unknown)'
            states.setdefault(state, set()).add(file_name)

    def _register_key_commands(self) -> None:
        """ Register key commands for opening/closing a directory/file. """
        super()._register_key_commands()

        c: Config = self._ctx.config
        right_key: int = c.get_key(Section.GLOBAL_KEYS, Field.RIGHT)
        left_key: int = c.get_key(Section.GLOBAL_KEYS, Field.LEFT)
        add_menu_key: int = c.get_key(Section.FILE_TREE_LAYOUT,
                                      Field.ADD_MENU_KEY)
        self.register_key_command(right_key, self.open)
        self.register_key_command(left_key, self.close)
        self.register_key_command(add_menu_key, self._open_add_menu)

    def _trigger_on_selected_cb(self) -> None:
        """
        Call the on-selected callback, if it exists.

        The path of the currently selected item is passed
        to the callback, or None if there is no data available.
        """
        if self._on_selected_cb is not None and self.selected >= 0:
            self._on_selected_cb(self.item.path)

    def _open_add_menu(self) -> None:
        """ Open the menu for adding files to the Git stage. """
        c: Config = self._ctx.config
        add_all_key: int = c.get_key(Section.FILE_TREE_LAYOUT,
                                     Field.ADD_ALL_FILES)
        add_key: int = c.get_key(Section.FILE_TREE_LAYOUT, Field.ADD_FILE)
        add_selected_key: int = c.get_key(Section.FILE_TREE_LAYOUT,
                                          Field.ADD_SELECTED_FILES)
        title: str = "Add file/s to the stage"
        add_all_title: str = 'Add all files (git add .)'
        add_current_title: str = 'Add this file (git add <path>)'
        add_selected_title: str = (
            'Add all selected files (git add <path> <path> ...)')
        add_menu_popup: CommandPopup = CommandPopup(self._ctx, title)
        add_menu_popup.register_key_command(
            add_selected_key,
            lambda: self._add(
                *filter(
                    lambda x: x,
                    self._flat_map(
                        lambda x: str(
                            x.path.absolute()) if x.marked else ''))),
            add_selected_title)
        add_menu_popup.register_key_command(add_all_key,
                                            lambda: self._add('.'),
                                            add_all_title)
        add_menu_popup.register_key_command(add_key, lambda: self._add(
            str(self.item.path)), add_current_title)
        add_menu_popup.show()

    def _add(self, *args) -> None:
        """
        Add files to the Git staging area.

        :param args: The paths as strings passed to the Git add command
        """

        def update_state(item: TreeView.Node) -> None:
            """
            Update the state of a tree view node.

            :param item: The tree node to be updated
            """
            item.state = self._get_node_state(item.path)

        if self._ctx.git is None:
            return
        try:
            self._ctx.git.exec('add', None, [], *args)
            self._init_changed_files()
            self._for_each(update_state)
            self._sync_internal_state()
        except git.CmdError as e:
            error_title = f"[❌] Command '{self._ctx.git.cmd}'"
            TextPopup(self._ctx, str(e), error_title).show()


class InfoView(TextView):
    """
    A view for showing the file/content diff and preview of a file node.

    :ivar _mode: The current mode, which determines the showed data
    :ivar _path: The path to the currently displayed file/directory
    """

    class Mode(Enum):
        """
        Different modes the view can be into.

        The mode determines what data will be shown
        for a file when calling `FileView.show()`.
        """
        LOG = 'log'
        DIFF = 'diff'
        PREVIEW = 'preview'

    def __init__(self, ctx: Context, mode: 'InfoView.Mode',
                 draw_border: bool = True):
        """
        :param mode: The default mode determining which information
                     of a file to show
        """
        super().__init__(ctx, None, None, draw_border)
        self._mode: InfoView.Mode = mode
        self._path: Optional[pathlib.Path] = None

        self._cg = [
            glue_regex([(COMMIT_REGEX, scheme.get_color(scheme.Type.NORMAL)),
                        (HASH_REGEX, scheme.get_color(scheme.Type.HASH))],
                       scheme.get_color(scheme.Type.NORMAL)),
            glue_regex([(AUTHOR_REGEX, scheme.get_color(scheme.Type.NORMAL)),
                        (NAME_REGEX, scheme.get_color(scheme.Type.NAME)),
                        (EMAIL_REGEX, scheme.get_color(scheme.Type.EMAIL))],
                       scheme.get_color(scheme.Type.NORMAL)),
            glue_regex([(AUTHOR_REGEX, scheme.get_color(scheme.Type.NORMAL)),
                        (
                            SINGLE_NAME_REGEX,
                            scheme.get_color(scheme.Type.NAME)),
                        (EMAIL_REGEX, scheme.get_color(scheme.Type.EMAIL))],
                       scheme.get_color(scheme.Type.NORMAL)),
            glue_regex([(DATE_REGEX, scheme.get_color(scheme.Type.NORMAL)),
                        (DATE_DEFAULT_REGEX,
                         scheme.get_color(scheme.Type.DATE))],
                       scheme.get_color(scheme.Type.NORMAL)),
            glue_regex([(ADDED_REGEX, scheme.get_color(scheme.Type.ADDED))],
                       scheme.get_color(scheme.Type.ADDED)),
            glue_regex(
                [(REMOVED_REGEX, scheme.get_color(scheme.Type.REMOVED))],
                scheme.get_color(scheme.Type.REMOVED))
        ]

    @property
    def path(self) -> Optional[pathlib.Path]:
        """
        Get the current path.

        :return: The current path or None if it does not exist
        """
        return self._path

    @path.setter
    def path(self, path: Optional[pathlib.Path]) -> None:
        """
        Set the path.

        :param path: The path to be set or None to unset
        """
        self._path = path

    def reload(self) -> None:
        """ Load data of the current file node depending on the mode. """
        if self._path:
            if self._mode == InfoView.Mode.DIFF:
                self._load_diff()
            if self._mode == InfoView.Mode.PREVIEW:
                self._load_preview()
            if self._mode == InfoView.Mode.LOG:
                self._load_log()

    def _register_key_commands(self) -> None:
        """ Register commands for switching between displaying modes. """

        def set_diff_mode() -> None:
            """ Set mode to show the diff of a file/directory. """
            self._mode = InfoView.Mode.DIFF
            self.reload()

        def set_log_mode() -> None:
            """ Set mode to show the commit history of a file/directory. """
            self._mode = InfoView.Mode.LOG
            self.reload()

        def set_preview_mode() -> None:
            """ Set mode to show the preview of a file/directory. """
            self._mode = InfoView.Mode.PREVIEW
            self.reload()

        c: Config = self._ctx.config
        log_key: int = c.get_key(Section.FILE_TREE_LAYOUT, Field.LOG_MODE_KEY)
        diff_key: int = c.get_key(Section.FILE_TREE_LAYOUT,
                                  Field.DIFF_MODE_KEY)
        preview_key: int = c.get_key(Section.FILE_TREE_LAYOUT,
                                     Field.PREVIEW_MODE_KEY)
        self.register_key_command(diff_key, set_diff_mode)
        self.register_key_command(log_key, set_log_mode)
        self.register_key_command(preview_key, set_preview_mode)

    def _load_log(self) -> None:
        """ Retrieve all commits for the current file. """
        if self._ctx.git is None or self._path is None:
            return

        try:
            self.title = f'Commit History: {get_pretty_path_name(self._path)}'
            log: List[str] = self._ctx.git.history(self._path, 5)

            if log:
                self._lines = log
            else:
                self.text = f'{get_pretty_path_name(self._path)} ' \
                            f'has not been committed yet.'
        except git.CmdError as e:
            self.text = str(e)

    def _load_diff(self) -> None:
        """ Retrieve the diff for the current file. """
        if self._ctx.git is None or self._path is None:
            return

        try:
            self.title = f'Diff: {get_pretty_path_name(self._path)}'
            diff: List[str] = self._ctx.git.diff(self._path)

            if diff:
                self._lines = diff
            else:
                self._lines = ['Nope, no diff to find here. Try again buddy.']
        except git.CmdError as e:
            self._lines = [str(e)]

    def _load_preview(self) -> None:
        """ Retrieve the content of the current file. """
        if self._path is None:
            return

        self.title = f'Preview: {get_pretty_path_name(self._path)}'

        if self._path.is_dir():
            text: str = ''

            for p in self._path.iterdir():
                text += f'{get_pretty_path_name(p)}\n'

            if not text:
                text = 'Directory is empty'

            self.text = text
        else:
            self.text = self._path.read_text()
