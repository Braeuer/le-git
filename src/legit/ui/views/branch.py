"""
A collection of views showing information of Git branches.

Views:
    1. ListView - Shows all local branches and provides interaction with them

Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""
from typing import Optional, List

from legit.config import Config, Section, Field
from legit.git import CmdError
from legit.ui.popup import InputPopup, TextPopup, CommandPopup, ConfirmPopup
from legit.ui.views.base import SelectView


class ListView(SelectView[SelectView.Item]):
    """
    A view for listing all branches within a repository.

    The following commands for a branch are supported in this view:
        1.  Checkout a new branch from existing branch
        2.  Checkout an existing branch
        3.  Delete a branch (normal and forced)
        4.  Rename a branch
    """

    def draw(self) -> None:
        """ Reload view if there are no file nodes and draw it. """
        if not self._items:
            self.reload()
        super().draw()

    def reload(self) -> None:
        """
        (Re-)initialize all files/directories within a directory.

        **Note**: Triggers the on-selected callback
        """
        if self._ctx.git is None:
            return

        self._items = [
            SelectView.Item(str(branch), False, False, 0, None, False) for
            branch in self._ctx.git.branches]

        if self._selected > len(self._items) - 1:
            self._selected = len(self._items) - 1

        self._sync_internal_state()

    def checkout_new_branch(self) -> None:
        """ Checkout a new git branch. """

        def on_enter_branch_name(name: str) -> None:
            """
            Callback when the branch name was entered.

            :param name: The name of the branch to be checked out
            """
            if self._ctx.git is None:
                return
            try:
                self._ctx.git.exec('checkout', None, ['-b'], name)
                self.reload()
            except CmdError as e:
                error_title = f"[❌] Command '{self._ctx.git.cmd}' failed"
                TextPopup(self._ctx, str(e), error_title).show()

        if self._ctx.git is None:
            return
        title: str = 'Enter the name of the new branch'
        InputPopup(self._ctx, title, on_enter_branch_name).show()

    def rename_branch(self) -> None:
        """ Rename the currently selected branch. """

        def on_enter_branch_name(name: str) -> None:
            """
            Callback when the branch name was entered.

            :param name: The name to rename the selected branch to
            """
            try:
                item: Optional[SelectView.Item] = self.item
                if item is not None:
                    self._ctx.git.exec('branch', None, ['-M'],
                                       item.display_name, name)
                    self.reload()
            except CmdError as e:
                error_title = f"[❌] Command '{self._ctx.git.cmd}' failed"
                TextPopup(self._ctx, str(e), error_title).show()

        if self._ctx.git is None:
            return

        title: str = 'Enter the new name of the branch'
        InputPopup(self._ctx, title, on_enter_branch_name).show()

    def checkout_branch(self) -> None:
        """ Checkout the currently selected branch. """
        if self._ctx.git is None:
            return
        try:
            item: Optional[SelectView.Item] = self.item
            if item:
                self._ctx.git.exec('checkout', None, [], item.display_name)
        except CmdError as e:
            error_title = f"[❌] Command '{self._ctx.git.cmd}' failed"
            TextPopup(self._ctx, str(e), error_title).show()

    def delete_branch(self, forced: bool = False) -> None:
        """
        Delete the currently selected branch.

        :param forced: Whether the branch should be deleted even when
                       it is not merged yet
        """
        if self._ctx.git is None:
            return

        options: List[str] = ['-D'] if forced else ['-d']
        try:
            item: Optional[SelectView.Item] = self.item
            if item:
                self._ctx.git.exec('branch', None, options, item.display_name)
                self.reload()
        except CmdError as e:
            error_title = f"[❌] Command '{self._ctx.git.cmd}' failed"
            TextPopup(self._ctx, str(e), error_title).show()

    def open_delete_menu(self) -> None:
        """ Open menu for deleting a branch. """
        if self._ctx.git is None:
            return

        c: Config = self._ctx.config
        d_key: int = c.get_key(Section.BRANCH_LAYOUT, Field.DELETE_BRANCH_KEY)
        f_key: int = c.get_key(Section.BRANCH_LAYOUT, Field.FORCE_DELETE_KEY)
        popup: CommandPopup = CommandPopup(
            self._ctx,
            f'Delete branch {self._ctx.git.branch}')

        popup.register_key_command(
            f_key,
            lambda: ConfirmPopup(self._ctx,
                                 (f'Are you sure you want to '
                                  f'delete {self.item.display_name}'),
                                 lambda: self.delete_branch(True)).show(),
            "Force-Delete selected branch ('git branch -D <branch>')")
        popup.register_key_command(
            d_key,
            lambda: self.delete_branch(False),
            "Delete selected branch ('git branch -d <branch>')")
        popup.show()

    def _register_key_commands(self) -> None:
        """ Register key commands for opening/closing a directory/file. """
        super()._register_key_commands()

        c: Config = self._ctx.config
        m_key: int = c.get_key(Section.BRANCH_LAYOUT, Field.RENAME_BRANCH_KEY)
        c_key: int = c.get_key(Section.BRANCH_LAYOUT, Field.CHECKOUT_NEW_KEY)
        s_key: int = c.get_key(Section.BRANCH_LAYOUT, Field.SWITCH_KEY)
        d_key: int = c.get_key(Section.BRANCH_LAYOUT,
                               Field.DELETE_BRANCH_MENU_KEY)

        self.register_key_command(c_key, self.checkout_new_branch)
        self.register_key_command(s_key, self.checkout_branch)
        self.register_key_command(d_key, self.open_delete_menu)
        self.register_key_command(m_key, self.rename_branch)
