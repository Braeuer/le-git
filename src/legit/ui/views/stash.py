"""
A collection of views showing information of the stash of a repository.

Views:
    1. ListView - Shows all stash entries of a repository
    2. DiffView - Shows the diff of a stash entry

Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""
from typing import List

from legit import git, scheme
from legit.config import Config, Section, Field
from legit.context import Context
from legit.ui.popup import TextPopup, InputPopup, ConfirmPopup, \
    CommandPopup, Popup
from legit.ui.render import glue_regex, STASH_INDEX_REGEX, \
    HASH_REGEX
from legit.ui.views.base import TextView, SelectView


class ListView(SelectView):
    """ A view for showing and interacting with all current stash entries. """

    def __init__(self, ctx: Context, draw_border: bool = True):
        super().__init__(ctx, None, None, draw_border)
        self._cg = [
            glue_regex(
                [(
                    STASH_INDEX_REGEX,
                    scheme.get_color(scheme.Type.STASH_INDEX)),
                    (HASH_REGEX, scheme.get_color(scheme.Type.HASH))],
                scheme.get_color(scheme.Type.NORMAL))
        ]

    def draw(self) -> None:
        """
        Show all stash entries or an info message if none is available.

        **Note**: Triggers the on-selected callback
        """
        if not self._lines:
            self.reload()
        super().draw()

    def reload(self) -> None:
        """
        Load all stash entries of the current repository.

        **Note**: Triggers the on-selected callback
        """
        if self._ctx.git is None:
            return

        try:
            output: List[str] = self._ctx.git.exec('stash', 'list', [])
            if output:
                self._lines = output
                items_count: int = len(self._lines)

                if self._selected >= items_count:
                    self._selected = items_count - 1
                elif self._selected < 0:
                    self._selected = 0
            else:
                self._selected = -1
                self._lines = ["No stash found! Looks like you're clean!"]
        except git.CmdError as e:
            self._selected = -1
            self._lines = [f'{str(e)}']

        self._trigger_on_selected_cb()

    def delete_stash(self) -> None:
        """ Delete the currently selected stash. """
        if self._ctx.git is None:
            return

        index: int = self._selected
        try:
            self._ctx.git.exec('stash', 'drop', [], f'stash@{{{index}}}')
            self.reload()
        except git.CmdError as e:
            TextPopup(self._ctx, str(e),
                      f'Could not delete stash@{{{index}}}').show()

    def clear_stash(self):
        """ Clear the whole stash. """
        if self._ctx.git is None:
            return

        try:
            self._ctx.git.exec('stash', 'clear', [])
            self.reload()
        except git.CmdError as e:
            TextPopup(self._ctx, str(e), 'Could not clear stash').show()

    def pop_stash(self) -> None:
        """ Pop the currently selected stash entry. """
        if self._ctx.git is None:
            return

        index: int = self._selected
        try:
            self._ctx.git.exec('stash', 'pop', [], f'stash@{{{index}}}')
            self._ctx.env.reload()
        except git.CmdError as e:
            TextPopup(self._ctx, str(e),
                      f'Could not pop stash@{{{index}}}').show()

    def create_branch_from_stash(self, name: str) -> None:
        """
        Create a new branch with the currently selected stash applied.

        :param name: The name of the branch to be created
        """
        if self._ctx.git is None:
            return

        index: int = self._selected
        try:
            self._ctx.git.exec('stash', 'branch', [], f'{name}',
                               f'stash@{{{index}}}')
            self._ctx.env.reload()
        except git.CmdError as e:
            title: str = (f'Could not create branch with name "'
                          f'{name}" from stash@{{{index}}}')
            TextPopup(self._ctx, str(e), title).show()

    def apply_stash(self) -> None:
        """ Apply the currently selected stash. """
        if self._ctx.git is None:
            return

        index: int = self._selected
        try:
            self._ctx.git.exec('stash', 'apply', [], f'stash@{{{index}}}')
            if self._ctx.env.selected:
                self._ctx.env.selected.reload()
        except git.CmdError as e:
            TextPopup(self._ctx, str(e),
                      f'Could not apply stash@{{{index}}}').show()

    def _register_key_commands(self) -> None:
        """
        Register key commands for managing stash entries/the stash.

        Key commands:
            1. Open delete menu
            2. Open apply menu
            3. Open input view for checking out a
               new branch from the current stash
        """
        super()._register_key_commands()

        c: Config = self._ctx.config
        del_key: int = c.get_key(Section.STASH_LAYOUT, Field.DELETE_MENU_KEY)
        apply_key: int = c.get_key(Section.STASH_LAYOUT, Field.APPLY_MENU_KEY)
        branch_key: int = c.get_key(Section.STASH_LAYOUT, Field.CHECKOUT_KEY)

        self.register_key_command(del_key, self._open_delete_menu)
        self.register_key_command(branch_key, self._open_new_branch_name_popup)
        self.register_key_command(apply_key, self._open_apply_menu)

    def _open_new_branch_name_popup(self) -> None:
        """
        Open a popup that asks for the branch name to be created.

        If stash is empty no popup will be displayed.
        """
        if self._selected < 0:
            return

        title: str = 'Enter a name for the branch to be created from stash'
        InputPopup(self._ctx, title,
                   self.create_branch_from_stash).show()

    def _open_clear_menu(self) -> None:
        """ Open a confirmation popup for clearing the whole stash. """
        title: str = 'Are you sure you do not need your stashed work anymore?'
        ConfirmPopup(self._ctx, title, self.clear_stash).show()

    def _open_delete_menu(self) -> None:
        """
        Open a popup showing all commands for deleting a stash.

        If stash is empty no popup will be displayed.

        Key commands:
            1. Clear stash
            2. Delete stash entry
            3. Pop stash entry
        """
        if self._selected < 0:
            return

        conf: Config = self._ctx.config
        c_key: int = conf.get_key(Section.STASH_LAYOUT, Field.CLEAR_KEY)
        d_key: int = conf.get_key(Section.STASH_LAYOUT, Field.DELETE_KEY)
        p_key: int = conf.get_key(Section.STASH_LAYOUT, Field.POP_KEY)

        title: str = f'Delete Stash@{{{self.selected}}}'
        popup: Popup = CommandPopup(self._ctx, title)
        popup.register_key_command(c_key, self._open_clear_menu, 'clear stash')
        popup.register_key_command(d_key, self.delete_stash, 'delete stash')
        popup.register_key_command(p_key, self.pop_stash, 'pop stash')
        popup.show()

    def _open_apply_menu(self) -> None:
        """
        Open a popup showing commands for applying the selected stash.

        If stash is empty no popup will be displayed.

        Key commands:
            1. Apply stash entry
            2. Pop stash entry
        """
        if self._selected < 0:
            return

        c: Config = self._ctx.config
        apply_key: int = c.get_key(Section.STASH_LAYOUT, Field.APPLY_KEY)
        pop_key: int = c.get_key(Section.STASH_LAYOUT, Field.POP_KEY)

        title: str = (f'Are you sure you want to apply '
                      f'Stash@{{{self._selected}}}?')

        popup: Popup = CommandPopup(self._ctx, title)
        popup.register_key_command(apply_key, self.apply_stash, 'apply stash')
        popup.register_key_command(pop_key, self.pop_stash, 'pop stash')
        popup.show()


class DiffView(TextView):
    """ A view for showing the file diff of a specific stash. """

    def __init__(self, ctx: Context, draw_border: bool = True):
        """
        :param ctx:         The context of the application
        :param draw_border: Whether a border should be drawn
        """
        super().__init__(ctx, None, None, draw_border)
        stashed_file_regex: str = r'[\S ]+'
        separator_regex: str = r' \|[ ]+\d+'
        changed_files_regex: str = r"\d+ files changed"
        self._cg = [
            glue_regex([
                (stashed_file_regex, scheme.get_color(scheme.Type.NORMAL)),
                (separator_regex, scheme.get_color(scheme.Type.NORMAL)),
                (r"\++", scheme.get_color(scheme.Type.ADDED)),
                (r"-+", scheme.get_color(scheme.Type.REMOVED)),
            ], scheme.get_color(scheme.Type.NORMAL)),
            glue_regex([
                (stashed_file_regex, scheme.get_color(scheme.Type.NORMAL)),
                (separator_regex, scheme.get_color(scheme.Type.NORMAL)),
                (r"\++", scheme.get_color(scheme.Type.ADDED)),
            ], scheme.get_color(scheme.Type.NORMAL)),
            glue_regex([
                (stashed_file_regex, scheme.get_color(scheme.Type.NORMAL)),
                (separator_regex, scheme.get_color(scheme.Type.NORMAL)),
                (r"-+", scheme.get_color(scheme.Type.REMOVED)),
            ], scheme.get_color(scheme.Type.NORMAL)),
            glue_regex([
                (changed_files_regex, scheme.get_color(scheme.Type.NORMAL)),
                (r"\+", scheme.get_color(scheme.Type.ADDED)),
                (r"-", scheme.get_color(scheme.Type.REMOVED)),
            ], scheme.get_color(scheme.Type.NORMAL)),
            glue_regex([
                (changed_files_regex, scheme.get_color(scheme.Type.NORMAL)),
                (r"\+", scheme.get_color(scheme.Type.ADDED))
            ], scheme.get_color(scheme.Type.NORMAL)),
            glue_regex([
                (changed_files_regex, scheme.get_color(scheme.Type.NORMAL)),
                (r"-", scheme.get_color(scheme.Type.REMOVED))
            ], scheme.get_color(scheme.Type.NORMAL))
        ]

    def load_file_diff_at(self, index: int) -> None:
        """
        Load the file diff for the stash at index.

        :param index: The index of the stash
        """
        if self._ctx.git is None:
            return

        if index < 0:
            self.title = 'Stash@{{...}}'
            self.text = ''
        else:
            self.title = f'Stash@{{{index}}}'
            try:
                self._lines = self._ctx.git.exec('stash', 'show', [],
                                                 f'stash@{{{index}}}')
            except git.CmdError as e:
                self.text = str(e)
