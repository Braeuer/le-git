"""
A collection of views showing repository related data.

Views:
    1. ListView - Lists all repositories
    2. PathView - Shows path of a repository

Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""
from pathlib import Path
from typing import Optional, List

from legit import environment, helper
from legit.config import Section, Field, Config
from legit.context import Context
from legit.git import Git
from legit.ui import views


class ListView(views.base.SelectView):
    """
    A view listing all repositories within a certain path.

    :ivar _path:         The directory path from where start the
                         search for repositories
    :ivar _repositories: The list of found repositories in `_path`
    """

    def __init__(self, path: Path, ctx: Context, draw_border: bool = True):
        """
        Search for repositories in path.

        :param path: The directory path from where to start
                     the search for repositories
        """
        super().__init__(ctx, None, None, draw_border)
        self._path: Path = path
        self._repositories: List[Path] = []
        self.reload()

    def reload(self) -> None:
        """ Find all repositories in `self._path`. """
        self._repositories = helper.find_all_repositories(self._path)
        self._lines = [str(p) for p in self._repositories]

    def choose_repository(self) -> None:
        """ Select the current repository and switch to the main view. """
        repo_path: Optional[Path] = self._repositories[self._selected]

        if repo_path:
            self._ctx.git = Git(repo_path)
            self._ctx.env.selected = environment.PageType.MAIN
            self._ctx.env.selected.reload()

    def _register_key_commands(self) -> None:
        """ Register key command for selecting a repository. """
        super()._register_key_commands()

        config: Config = self._ctx.config
        right_key: int = config.get_key(Section.GLOBAL_KEYS, Field.RIGHT)
        self.register_key_command(right_key, self.choose_repository)


class PathView(views.base.TextView):
    """ A view for showing the current repository path and branch. """

    def __init__(self, ctx: Context, draw_border: bool = True):
        """
        :param ctx:         The context of the application
        :param draw_border: Whether a border should be drawn
        """
        super().__init__(ctx, None, None, draw_border)

    def draw(self) -> None:
        """ Draw the path and active branch of the selected repository. """
        self.text = f'{self._ctx.git.path} > {self._ctx.git.branch}'
        super().draw()
