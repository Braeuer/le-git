"""
A collection of base views to be extended or used directly.

Views:
    1. View       - Provides generic functionality
    2. TextView   - Renders text in a simple manner
    3. SelectView - Renders a list of items to be selectable
    4. TabView    - Renders names of tabs that can be highlighted

Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""
import curses
from abc import ABC
from dataclasses import dataclass
from typing import Optional, List, Callable, TypeVar, Generic

from legit.config import Config, Section, Field
from legit.context import Context
from legit.handler import KeyCommandHandler
from legit.ui.base import Drawable

T = TypeVar('T')
K = TypeVar('K')
OnScrollView = Callable[..., None]  #: Callback for scrolling a view


class View(Drawable, KeyCommandHandler, ABC):
    """ A base class for all views. """

    def __init__(self, ctx: Context, text: Optional[str] = None,
                 title: Optional[str] = None, draw_border: bool = True):
        """
        :param ctx:         The context of the application
        :param draw_border: Whether a border should be drawn
        """
        Drawable.__init__(self, ctx, text, title, draw_border)
        KeyCommandHandler.__init__(self)

    def set_relative_size(self, parent: curses.window, rows: int,
                          cols: int, y: int, x: int) -> None:
        """
        Set the size of the view relative to the passed parent window.

        **Note**: The number of rows + y **must not** be greater
                  than the number of rows of the parent
        **Note**: The number of columns + x **must not** be greater
                  than the number of columns of the parent

        :param parent: The parent window
        :param rows:   The number of rows to be spanned (>= 3)
        :param cols:   The number of columns to be spanned (>= 3)
        :param y:      The vertical position relatively to
                       the top of the window (>= 0)
        :param x:      The horizontal position relatively to
                       the left of the window (>= 0)
        """
        self._rows = rows - 2
        self._cols = cols - 2 - 2 * self._x
        self._border_box = parent.derwin(rows, cols, y, x)
        self._window = self._border_box.derwin(self._rows, self._cols, 1, 1)

    def reload(self) -> None:
        """
        Reload the data of the view.

        This function should be used to fetch new data from disk.
        If there is no data to be fetched this method can remain empty.
        """


class TextView(View):
    """ A view for rendering scrollable but non-selectable text. """

    def scroll_up(self) -> None:
        """ Scroll the whole view content up. """
        if self._y_start > 0:
            self._y_start -= 1

    def scroll_down(self) -> None:
        """ Scroll the whole view content down. """
        if self._y_start < len(self._lines) - 1:
            self._y_start += 1

    def _register_key_commands(self) -> None:
        """ Register key commands for scrolling the view up/down. """
        c: Config = self._ctx.config

        up_key: int = c.get_key(Section.GLOBAL_KEYS, Field.SCROLL_UP)
        down_key: int = c.get_key(Section.GLOBAL_KEYS, Field.SCROLL_DOWN)
        self.register_key_command(down_key, self.scroll_down)
        self.register_key_command(up_key, self.scroll_up)


class SelectView(View, Generic[T]):
    """
    A scrollable view for showing selectable items.

    :ivar _items:            The list of items to be rendered
    :ivar _scroll_threshold: Maximum cursor distance from bottom/top at
                             which to start scrolling the whole view.
                             The threshold is dependent of the current
                             height of the view (height // 4).
    :ivar _on_selected_cb:   A callback which is executed when an item
                             was selected.
    """

    @dataclass
    class Item:
        """
        :ivar display_name: The string to be displayed for this item
        :ivar selected:     Flag whether this item is selected.
                            **Note**: Reserved for future usage
        :ivar marked:       Flag whether the item is marked and will be used
                            in a suitable bulk-operation
        :ivar level:        The level in the file tree (repository root = 0)
        :ivar children:     A list of child file nodes. If file node is not
                            a directory this field **must not** be used.
        :ivar expanded:     An indicator whether the file node is expanded
                            and the children should be processed/rendered
        """
        display_name: str
        selected: bool
        marked: bool
        level: int
        children: Optional[List[T]]
        expanded: bool

    def __init__(self, ctx: Context, text: Optional[str] = None,
                 title: Optional[str] = None, draw_border: bool = True):
        super().__init__(ctx, text, title, draw_border)

        self._items: List[T] = []
        self._scroll_threshold: int = 0
        self._on_selected_cb: Optional[OnScrollView] = None
        self._selected = 0

    @property
    def items(self) -> List[str]:
        """
        Get the currently displayed items.

        :return: The list of items
        """
        return self._lines

    @items.setter
    def items(self, items: List[str]) -> None:
        """
        Replace the currently displayed items.

        **Note**: Setting the text of the view, where all items are
                  separated by a `\n` from each other is equivalent
                  to setting the items directly.

        :param items: A list of items
        """
        self._lines = items

    @property
    def selected(self) -> int:
        """
        Get the index of the currently selected item.

        :return: The index of the selected item or -1 if
                 there is no item selected/available
        """
        return self._selected

    @property
    def item(self) -> Optional[T]:
        """
        Get the currently selected item.

        :return: The selected item or None if none is selected
        """
        item: Optional[SelectView.Item] = None
        index: int = -1
        items: List[T] = list(self._items)

        while index != self._selected:
            index += 1
            item = items.pop(0)
            if item.children:
                items = list(item.children) + items

        return item

    def set_relative_size(self, parent: curses.window, rows: int, cols: int,
                          y: int,
                          x: int) -> None:
        super().set_relative_size(parent, rows, cols, y, x)
        self._scroll_threshold = self._rows // 4

    def register_on_selected_callback(self, cb: OnScrollView) -> None:
        """
        Register a callback that will be called when an item is selected.

        **Note**: The `cb` function must take exactly one argument
                  from type `int` (see `self._trigger_on_selected_cb`)

        :param cb: The callback to be called when the view selects a item
        """
        self._on_selected_cb = cb

    def scroll_up(self) -> None:
        """
        Select the previous item and scroll up the view if necessary.

        Depending on the position of the selected item and the scroll
        threshold, not only the index of the selected item will be
        decreased but also the index of the first item to be rendered,
        which means that the view will scroll up. This gives a better
        user experience, since the view scrolls up before selecting the
        currently top-most item and therefore the user can always see
        what items come next.

        The number of items 'previewed' are set in 'self._scroll_threshold'.

        **Note**: Triggers the on-selected callback
        """
        if self._selected > 0:
            self._selected -= 1

            if self._selected > self._scroll_threshold and self._y_start > 0:
                self._y_start -= 1

            self._trigger_on_selected_cb()

    def scroll_down(self) -> None:
        """
        Select the next item and scroll down the view if necessary.

        Depending on the position of the selected item and the scroll
        threshold, not only the index of the selected item will be
        increased but also the index of the first item to be rendered,
        which means that the view will scroll down. This gives a better
        user experience, since the view scrolls down before selecting
        the currently bottom-most displayed item and therefore the user
        can always see what items come next.

        The number of items 'previewed' are set in 'self._scroll_threshold'.

        **Note**: Triggers the on-selected callback
        """
        if self._selected < 0:
            return

        rows, cols = self._window.getmaxyx()
        count: int = len(self._lines)

        if self._selected < count - 1:
            self._selected += 1

            if self._selected - self._y_start > rows - self._scroll_threshold:
                self._y_start += 1

            self._trigger_on_selected_cb()

    def toggle_mark(self) -> None:
        """ (Un-)mark the currently selected item for later actions. """
        item: SelectView.Item = self.item

        if item is not None:
            item.marked = not item.marked
            self._sync_internal_state()

    def _register_key_commands(self) -> None:
        """ Register key commands for selecting the next/previous item. """
        c: Config = self._ctx.config
        up_key: int = c.get_key(Section.GLOBAL_KEYS, Field.UP)
        down_key: int = c.get_key(Section.GLOBAL_KEYS, Field.DOWN)
        toggle_mark_key: int = c.get_key(Section.GLOBAL_KEYS,
                                         Field.TOGGLE_MARK)

        self.register_key_command(up_key, self.scroll_up)
        self.register_key_command(down_key, self.scroll_down)
        self.register_key_command(toggle_mark_key, self.toggle_mark)

    def _trigger_on_selected_cb(self) -> None:
        """
        Call the on-selected callback, if it exists.

        The index of the currently selected item is passed to
        the callback, or -1 if there is no data available.
        """
        if self._on_selected_cb is not None:
            self._on_selected_cb(self._selected)

    def _sync_internal_state(self) -> None:
        """ Synchronize the internal state for rendering. """
        self._lines = self._flat_map(lambda item: item.display_name)

    def _for_each(self, fun: Callable[[T], None]) -> None:
        """
        Execute a function for each visible item of the view.

        :param fun: The function to be executed
        """
        open_list: List[SelectView.Item] = list(self._items)

        while open_list:
            item: SelectView.Item = open_list.pop(0)
            fun(item)

            if item.expanded and item.children:
                open_list = list(item.children) + open_list

    def _flat_map(self, fun: Callable[[T], K]) -> List[K]:
        """
        Map each item to another object and flatten the tree to a list.

        :param fun: The transformation function to be called
                    for each item
        :return:    A list containing the mapped objects
        """
        mapped_list: List[K] = []
        open_list: List[SelectView.Item] = list(self._items)

        while open_list:
            item: SelectView.Item = open_list.pop(0)
            mapped_list.append(fun(item))

            if item.expanded and item.children:
                open_list = list(item.children) + open_list

        return mapped_list


class TabView(TextView):
    """
    A view for showing all available layouts as tabs.

    :ivar _icons:       The icons for the tabs to be displayed
    :ivar _highlighted: The currently highlighted tab
    """

    def __init__(self, ctx: Context, draw_border: bool = True):
        super().__init__(ctx, None, None, draw_border)
        self._icons: List[str] = []
        self._highlighted: int = 0
        self.highlight_at(self._highlighted)

    def add_icon(self, icon: str) -> None:
        """ Add a tab icon to the view. """
        self._icons.append(icon)

    def remove_icon(self, icon: str) -> None:
        """
        Remove a tab icon from the view.

        **Note*: This only removes the first occurrence.
        **Note**: The index of the currently highlighted
                  icon is not changed.
        """
        self._icons.remove(icon)

    def highlight_at(self, index: int) -> None:
        """
        Highlight the layout at a specific index.

        **Note**: If no icon for the specified index
                  is available, nothing will be done.
        """
        if 0 <= index < len(self._icons):
            self._highlighted = index
            text: str = ''
            for i in range(0, len(self._icons)):
                if i == self._highlighted:
                    text += f'[ {self._icons[i]} ]'
                else:
                    text += f'  {self._icons[i]}  '
            self.text = text
