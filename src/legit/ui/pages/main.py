"""
Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""
import pathlib
from typing import Optional

from legit import environment
from legit.config import Section, Field, Config
from legit.context import Context
from legit.git import Git, CredentialsType, CmdError
from legit.helper import get_text_from_editor
from legit.ui import layout, views
from legit.ui.layout import TabbedLayout
from legit.ui.pages import base
from legit.ui.popup import CommandPopup, InputPopup, TextPopup, LoadingPopup, \
    Popup


class Page(base.Page):
    """
    Create the main page.

    It consists of layouts showing the following data:
        1.  Project Files
        2.  Stash
    """

    def __init__(self, ctx: Context):
        """
        Create the main page.

        It consists of a tabbed layout, that contains:
            1.
            2.  self._create_stash_layout

        :return: The main page
        """
        tabbed_layout: layout.TabbedLayout = TabbedLayout(ctx)
        super().__init__(ctx, tabbed_layout)

        conf: Config = ctx.config
        s_key: int = conf.get_key(Section.GLOBAL_KEYS, Field.START_MENU)
        p_key: int = conf.get_key(Section.REPOSITORY_SET, Field.PUSH_MENU_KEY)
        u_key: int = conf.get_key(Section.REPOSITORY_SET, Field.PULL_MENU_KEY)
        status_key: int = conf.get_key(Section.REPOSITORY_SET,
                                       Field.STATUS_KEY)
        commit_key: int = conf.get_key(Section.REPOSITORY_SET,
                                       Field.COMMIT_KEY)
        f_icon: str = conf.get_string(Section.FILE_TREE_LAYOUT, Field.ICON)
        b_icon: str = conf.get_string(Section.BRANCH_LAYOUT, Field.ICON)
        s_icon: str = conf.get_string(Section.STASH_LAYOUT, Field.ICON)

        file_layout: layout.Layout = self._create_file_layout()
        stash_layout: layout.Layout = self._create_stash_layout()
        branch_layout: layout.Layout = self._create_branch_layout()
        tabbed_layout.add_layout(f_icon, file_layout)
        tabbed_layout.add_layout(b_icon, branch_layout)
        tabbed_layout.add_layout(s_icon, stash_layout)
        tabbed_layout.register_key_command(s_key, self.switch_to_start)
        tabbed_layout.register_key_command(p_key, self.open_push_menu)
        tabbed_layout.register_key_command(u_key, self.open_pull_menu)
        tabbed_layout.register_key_command(status_key, self.show_status)
        tabbed_layout.register_key_command(commit_key, self.commit)

    def _create_branch_layout(self) -> layout.Layout:
        """
        A layout for interacting with the local branches of a repository.

        The layout is a split layout consisting of the following views:
            1. Top:   views.repository.PathView
            2. Left:  views.branch.ListView
        """
        repo_view: views.repository.PathView

        repo_view = views.repository.PathView(self._ctx, False)
        list_view: views.branch.ListView = views.branch.ListView(self._ctx)
        list_view.title = 'Branches'

        return layout.SplitLayout(self._ctx, repo_view, None, list_view, None)

    def _create_stash_layout(self) -> layout.Layout:
        """
        A layout for interacting with the file system of a project.

        The layout is a split layout consisting of the following views:
            1. Top:   views.repository.PathView
            2. Left:  views.file.TreeView
            3. Right: views.file.InfoView
        """
        repo_view: views.repository.PathView
        repo_view = views.repository.PathView(self._ctx, False)
        diff_view: views.stash.DiffView = views.stash.DiffView(self._ctx)
        diff_view.title = 'Stash@{...}'
        list_view: views.stash.ListView = views.stash.ListView(self._ctx)
        list_view.title = 'Stash'
        list_view.register_on_selected_callback(diff_view.load_file_diff_at)

        return layout.SplitLayout(self._ctx, repo_view, None, list_view,
                                  diff_view)

    def _create_file_layout(self) -> layout.Layout:
        """
        A layout for interacting with the file system of a project.

        The layout is a split layout consisting of the following views:
            1. Top:   views.repository.PathView
            2. Left:  views.file.TreeView
            3. Right: views.file.InfoView
        """

        def set_file_view_path(path: Optional[pathlib.Path]) -> None:
            """
            Set the path of the file view.

            :param path: The path to be set or None to unset
            """
            info_view.path = path
            info_view.reload()

        mode: views.file.InfoView.Mode = views.file.InfoView.Mode(
            self._ctx.config.get_string(Section.FILE_TREE_LAYOUT,
                                        Field.DEFAULT_MODE))

        repo_view: views.repository.PathView
        repo_view = views.repository.PathView(self._ctx, False)
        tree_view: views.file.TreeView = views.file.TreeView(self._ctx)
        info_view: views.file.InfoView = views.file.InfoView(self._ctx, mode)

        file_layout: layout.SplitLayout
        file_layout = layout.SplitLayout(self._ctx, repo_view, None,
                                         tree_view, info_view)

        tree_view.title = 'Files'
        info_view.title = 'File: ...'
        tree_view.register_on_selected_callback(set_file_view_path)

        return file_layout

    def switch_to_start(self) -> None:
        """ Switch to start layout. """
        self._ctx.env.selected = environment.PageType.START

    def commit(self) -> None:
        """
        Commit staged files.

        This message opens the system editor for entering
        the commit message.
        """
        if self._ctx.git is None:
            return

        initial_text: str = (f"\n# Please enter the commit message for your "
                             f"changes. Lines starting\n# with '#' will be "
                             f"ignored, and an empty message aborts the "
                             f"commit.\n#\n# On branch {self._ctx.git.branch}")
        try:
            result: Optional[str] = get_text_from_editor(initial_text)
            commit_msg: str = ''
            self._ctx.root_window.redrawwin()
            if result is not None:
                for line in result.split('\n'):
                    if not line.startswith('#'):
                        commit_msg += f'{line}\n'
                if commit_msg.strip():
                    self._ctx.git.exec('commit', None, ['-m'], commit_msg)
        except CmdError as e:
            result = str(e)
            title = f"[❌] Command '{self._ctx.git.cmd}' failed"
            TextPopup(self._ctx, result, title).show()

    def show_status(self) -> None:
        """ Show the Git status in a popup. """
        if self._ctx.git is None:
            return

        title: str
        result: str

        try:
            result = '\n'.join(self._ctx.git.exec('status', None, []))
            title = f"[✅] Command '{self._ctx.git.cmd}' successful"

        except CmdError as e:
            result = str(e)
            title = f"[❌] Command '{self._ctx.git.cmd}' failed"

        TextPopup(self._ctx, result, title).show()

    def force_pull(self) -> None:
        """ Do a force pull from remote branch with the same name. """
        if self._ctx.git is None:
            return
        self._ctx.git.exec_remote('pull', None, self.on_credentials,
                                  self.on_spawn, self.on_complete, ['-f'])

    def pull(self) -> None:
        """ Pull changes of current branch from remote. """
        if self._ctx.git is None:
            return
        self._ctx.git.exec_remote('pull', None, self.on_credentials,
                                  self.on_spawn, self.on_complete, [])

    def force_push(self) -> None:
        """
        Do a force push to remote branch with the same name.

        **Note**: This will also set `branch` as upstream branch
        """
        if self._ctx.git is None:
            return

        branch: str = self._ctx.git.branch
        self._ctx.git.exec_remote('push', None, self.on_credentials,
                                  self.on_spawn, self.on_complete,
                                  ['-f', '-u', 'origin', f'{branch}'])

    def push(self) -> None:
        """
        Push commits of current branch to remote branch with the same name.

        **Note**: This will also set `branch` as upstream branch
        """
        if self._ctx.git is None:
            return

        branch: str = self._ctx.git.branch
        self._ctx.git.exec_remote('push', None, self.on_credentials,
                                  self.on_spawn, self.on_complete,
                                  ['-u', 'origin', f'{branch}'])

    def on_credentials(self, git: Git, c: CredentialsType) -> None:
        """
        Show an input popup for entering credentials.

        :param git: The current git object
        :param c:   The type of credential to be entered
        """
        text: str
        remote: str = git.remote
        hidden: bool = True

        if c == CredentialsType.SSH_KEY:
            if git.ssh_retry:
                text = 'Passphrase incorrect, please try again:'
            else:
                text = "Please enter your SSH key's passphrase:"
        elif c == CredentialsType.USERNAME:
            text = f"Please enter your username for '{remote}':"
            hidden = False
        elif c == CredentialsType.PASSWORD:
            text = f"Please enter your password for '{remote}':"
        else:
            return

        def on_enter(t: str) -> None:
            """ Show loading popup and send text to git process. """
            self.on_spawn(git)
            git.send(t)

        InputPopup(self._ctx, text, on_enter, hidden).show()
        self._ctx.env.redraw()

    def on_complete(self, git: Git, exit_status: int, text: str) -> None:
        """
        Show output of generic `git` command.

        :param git:         The current git object
        :param exit_status: The exit status of the command
        :param text:        The output of the command
        """
        title: str

        if exit_status == 0:
            title = f"[✅] Command '{git.cmd}' successful"
        else:
            title = f"[❌] Command '{git.cmd}' failed"

        TextPopup(self._ctx, text.lstrip(), title).show()
        self._ctx.env.redraw()

    def on_spawn(self, git: Git) -> None:
        """
        Show a popup displaying the current git command.

        :param git: The current git object
        """
        LoadingPopup(self._ctx, f"'{git.cmd}'", git.cancel).show()
        self._ctx.env.redraw()

    def open_pull_menu(self) -> None:
        """ Open a menu showing all supported pull operations. """
        conf: Config = self._ctx.config
        p_key: int = conf.get_key(Section.REPOSITORY_SET, Field.PULL_KEY)
        f_key: int = conf.get_key(Section.REPOSITORY_SET, Field.FORCE_PULL_KEY)

        popup: Popup = CommandPopup(self._ctx, 'Pull Changes')
        popup.register_key_command(p_key, self.pull, 'pull')
        popup.register_key_command(f_key, self.force_pull, 'force pull')
        popup.show()

    def open_push_menu(self) -> None:
        """ Open a menu showing all supported push operations. """
        conf: Config = self._ctx.config
        p_key: int = conf.get_key(Section.REPOSITORY_SET, Field.PUSH_KEY)
        f_key: int = conf.get_key(Section.REPOSITORY_SET, Field.FORCE_PUSH_KEY)
        popup: Popup = CommandPopup(self._ctx, 'Push Changes')
        popup.register_key_command(p_key, self.push, 'push')
        popup.register_key_command(f_key, self.force_push, 'force push')
        popup.show()
