"""
Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""

from legit import handler
from legit.context import Context
from legit.ui.layout import Layout


class Page(handler.KeyCommandHandler):
    """
    A base class for pages to be rendered.

    A page is the most top-level ui element in LeGit,
    which contains exactly one layout and is directly
    managed by the environment.

    :ivar _ctx:    The context of the application
    :ivar _layout: The layout of the page
    """

    def __init__(self, ctx: Context, layout: Layout):
        """
        :param ctx:    The context of the application
        :param layout: The layout of the page
        """
        super().__init__()

        self._ctx: Context = ctx
        self._layout: Layout = layout

    def draw(self) -> None:
        """ Apply this page to the viewport. """
        self._layout.draw()

    def reload(self) -> None:
        """ Reload the data of the layout. """
        self._layout.reload()

    def resize(self) -> None:
        """ Resize the current page. """
        rows, cols = self._ctx.root_window.getmaxyx()

        self._layout.set_size(self._ctx.root_window, rows - 2, cols - 2, 1, 1)

    def handle_command(self, key: int) -> bool:
        return super().handle_command(key) or self._layout.handle_command(key)
