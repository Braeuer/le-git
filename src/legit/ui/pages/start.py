"""
Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""

import pathlib

from legit.context import Context
from legit.ui.layout import SplitLayout
from legit.ui.pages import base
from legit.ui.views import repository


class Page(base.Page):
    """
    Create the start page.

    It consists of the following views:
        1.  views.repository.ListView
    """

    def __init__(self, ctx: Context, path: pathlib.Path):
        """
        Initialize all views/layouts of the start page.

        :param ctx:  The context of the application
        :param path: The directory path from where to start
                     the search for repositories
        """
        repos_view: repository.ListView
        repos_view = repository.ListView(path, ctx)
        super().__init__(ctx, SplitLayout(ctx, None, None, repos_view, None))
