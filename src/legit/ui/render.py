"""
Author: Matthias Bräuer
License: MIT
"""
import curses
import re
from typing import Optional, List, Tuple, Pattern

from legit import scheme

ColorFilter = Tuple[str, int]
ColorFilterSequence = List[ColorFilter]
ColorFilterSequenceGroup = List[Tuple[ColorFilterSequence]]
ColorSequence = List[Tuple[str, int]]
ColorGroupElement = Tuple[Pattern, List[int]]
ColorGroup = List[ColorGroupElement]

COMMIT_REGEX: str = r"commit"
AUTHOR_REGEX: str = r"Author"
DATE_REGEX: str = r"Date"
DATE_DEFAULT_REGEX: str = (
    r"(?:[a-zA-Z]{3} ){2}\d{1,2} (?:\d{2}:){2}\d{2} \d{4} \+\d{4}"
)
HASH_REGEX: str = r"[0-9a-f]{5,40}"
ADDED_REGEX: str = r"\+.*"
REMOVED_REGEX: str = r"\-.*"
ANY_REGEX: str = r'.*'
NON_GREEDY_ANY_REGEX: str = r'.*?'
WHITESPACE_REGEX: str = r'\s*'
EMAIL_REGEX: str = r"[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+"
NAME_REGEX: str = r"\w+ \w+"
SINGLE_NAME_REGEX: str = r"\w+"
STASH_INDEX_REGEX: str = r"stash@\{\d+\}"


def glue_regex(cfs: ColorFilterSequence,
               default_color: int) -> ColorGroupElement:
    """
    Glue together multiple color filter sequences into one RegEx.

    Each color filter sequence is converted to a match-group and
    will be represented by a color -> Color filter sequences **must**
    only contain non-capturing match groups.
    """
    colors: List[int] = [default_color]
    reg = f'({WHITESPACE_REGEX})'

    for f in cfs:
        reg += f'({f[0]})({NON_GREEDY_ANY_REGEX})'
        colors.append(f[1])
        colors.append(default_color)

    reg += f'({ANY_REGEX})'
    colors.append(default_color)

    return re.compile(reg), colors


def extract_colors(text: str, default_color: int,
                   cg: ColorGroup) -> ColorSequence:
    """
    Match a text by a color group and create a color sequence for it.

    If the color group matches the whole text, the colors within the
    color group determines the color of each matched group within the
    text. If the color group does not match the whole text, the color
    sequence will contain exactly one element tagged with the given
    default color.

    :param text:          The text to be matched by the color group
    :param default_color: The default color to be used when the color
                          group does not match the text
    :param cg:            The color group to match the given text
    :return:              The created color sequence
    """
    sequence: List = []

    for g in cg:
        match = re.match(g[0], text)
        if match:
            l: List = list(match.groups())
            length: int = len(l)
            for i in range(0, length):
                sequence.append((l[i], g[1][i]))
            break

    return sequence if sequence else [(text, default_color)]


def colored_line(window: curses.window, text: str, max_cols: int, y: int,
                 x: int, x_start: int, default_color: int, highlighted: bool,
                 cg: ColorGroup) -> None:
    """
    Render a line with colors determined by the give color group.

    :param window:        The window to render the line into
    :param text:          The text to be rendered
    :param max_cols:      The maximum number of columns to be filled
    :param y:             The number of the row to render the text into
    :param x:             The number of the column to start rendering
                          the text into
    :param x_start:       The index of the first character of the text
                          to be rendered (e.g. a start index 1 means
                          that the first character of the text will not
                          be rendered)
    :param default_color: The color to be used when the color group does
                          not match the text
    :param highlighted:   Whether the highlighted version of all colors
                          should be used
    :param cg:            The color group to match the text
    """
    sequences: ColorSequence = extract_colors(text, default_color, cg)
    color: int
    s: str

    for seq in sequences:
        s = seq[0]
        color = seq[1] if not highlighted else scheme.convert_to_highlighted(
            seq[1])
        window.insnstr(y, x, s, max_cols, color)
        x += len(s)

    if x < max_cols:
        color = (default_color if not highlighted
                 else scheme.convert_to_highlighted(default_color))
        window.insnstr(y, x, ' ' * (max_cols - x), max_cols, color)


def line(window: curses.window, text: str, max_cols: int,
         y: int, x: int, x_start: int, color: int) -> None:
    """
    Render a line at a specific position with a specific color scheme.

    :param window:   The window to render the text into
    :param text:     The line to be rendered
    :param max_cols: The maximal number of characters to be written (> 0)
    :param y:        The row of the window to render the line into (>= 0)
    :param x:        The column of window to render the line into (>= 0)
    :param x_start:  The index of the first character of a line to be
                     rendered (>= 0)

    :param color:    The curses color pair to draw the line with
    """
    assert max_cols > 0 and y >= 0 and x >= 0 and x_start >= 0

    padding: str = ' ' * max_cols
    text += padding
    window.insnstr(y, x, text[x_start:], max_cols, color)


def title(window: curses.window, text: str, cols: int, color: int,
          div_color: Optional[int] = None) -> int:
    """
    Draw a title with a horizontal divider below.

    The title itself is assumed to be a single line without line-breaks.

    :param window:    The window to draw into
    :param text:      The title to be drawn
    :param cols:      The columns to be written into (> 0). See
                      `render.line()` for drawing behavior when the title
                      length is smaller than the number of columns.
    :param color:     The curses color to paint the title
    :param div_color: The curses color to paint the divider.
                      If `None`, no divider will be drawn.
    :return:          The number of rendered rows
    """
    assert cols > 0

    line(window, text, cols, 0, 0, 0, color)

    if div_color:
        window.hline(1, 0, curses.ACS_HLINE, cols, div_color)
        return 2

    return 1


def lines(window: curses.window, text: List[str], selected: int,
          c: int,
          cg: Optional[ColorGroup],
          rows: int, cols: int,
          y: int, x: int, y_start: int, x_start: int) -> None:
    """
    Render a list of lines within a window.

    :param window:      The window to render into
    :param text:        The lines to be rendered
    :param selected:    The line to be rendered with highlighted color
    :param c:           The normal color
    :param cg:          The color group to match each line and determine
                        the color of different parts of the matched line
    :param rows:        The number of rows to be rendered (> 0). If
                        greater than the number of lines, empty lines
                        will be rendered into the remaining rows.
    :param cols:        The number of columns to be rendered (> 0).
                        See `render.line()` for drawing behavior when
                        the length of a line is smaller than the number
                        of columns.
    :param y:           The vertical padding relatively to the top of
                        the window to start rendering the lines (>= 0).
    :param x:           The horizontal padding from the left to start
                        drawing a line (>= 0). Setting a horizontal
                        padding will When a horizontal padding is set,
                        the line will be prefixed by spaces and
    :param y_start:     The index of the first line to be rendered (>= 0)
    :param x_start:     The index of the first character of a line to be
                        rendered (>= 0)
    """
    left_padding: str = ' ' * x
    color: int
    item: str
    item_index: int
    draw_index: int
    highlighted: bool

    assert rows > 0 and cols > 0
    assert y >= 0 and x >= 0 and y_start >= 0 and x_start >= 0

    for i in range(0, rows):
        item_index = i + y_start
        draw_index = i + y

        if item_index >= len(text):
            window.move(draw_index, 0)
            window.clrtoeol()
            continue
        highlighted = item_index == selected
        item = f'{left_padding}{text[item_index]}'

        if cg is None:
            color = c if not highlighted else scheme.convert_to_highlighted(c)
            line(window, item, cols, draw_index, 0, x_start, color)
        else:
            colored_line(window, item, cols, draw_index, 0, x_start, c,
                         highlighted, cg)
