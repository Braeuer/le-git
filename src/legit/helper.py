"""
A collection of useful functions.

Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""
import os
import subprocess
import tempfile
from pathlib import Path
from typing import List, Optional


def find_all_repositories(path: Path) -> List[Path]:
    """
    Find all repositories within a directory.

    :param path: The path where to start looking
    :return: The list of found repository Paths
    """
    git_repo_paths: List[Path] = []
    open_list: List[Path] = list(path.iterdir())
    elem: Path

    while open_list:
        try:
            elem = open_list.pop(0)

            if elem.is_dir() and not elem.match('.*'):
                elements: List[Path] = list(elem.iterdir())
                is_git_dir: bool = False

                for e in elements:
                    if e.match('.git'):
                        git_repo_paths.append(elem)
                        is_git_dir = True
                        break

                if not is_git_dir:
                    open_list += elements
        except PermissionError:
            pass

    return git_repo_paths


def open_file_in_editor(path: Path) -> None:
    """
    Open an existing file in the system's editor.

    If there is no editor set in the system, try to
    open the file in vim.

    :param path The path to the file to be edited
    """
    editor = os.environ.get('EDITOR', 'vim')

    subprocess.call([editor, str(path)])


def get_text_from_editor(initial_text: str = '') -> Optional[str]:
    """
    Open a temporary file in the default editor and return the entered text.

    :param initial_text: The text to be displayed when the editor is
                         opened
    :return:             The entered text (including the initial text
                         if it was not removed) or None if none was
                         entered
    """
    editor = os.environ.get('EDITOR', 'vim')
    with tempfile.NamedTemporaryFile(suffix=".tmp") as tmp:
        tmp.write(initial_text.encode())
        tmp.flush()
        subprocess.call([editor, tmp.name])
        tmp.seek(0)
        result: str = tmp.read().decode()
        return result if result != initial_text else None
