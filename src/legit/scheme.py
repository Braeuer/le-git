"""
Module containing different color schemes for LeGit.

Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""
import curses

# Curses provides colors with ID 0-7
from enum import IntEnum

COLOR_GRAY: int = 8
COLOR_GOLD: int = 9
COLOR_PURPLE: int = 10
COLOR_ORANGE: int = 11
COLOR_RED: int = 12
COLOR_GREEN: int = 13
COLOR_BLUE: int = 14
COLOR_LIGHT_ORANGE: int = 15
COLOR_LIGHT_BLUE: int = 16

ADD: int = 20


class Type(IntEnum):
    """ Enumeration of IDs of different semantic types of color. """
    NORMAL = 1,
    DIVIDER = 2,
    HASH = 3,
    ADDED = 4,
    REMOVED = 5,
    DATE = 6,
    NAME = 7,
    EMAIL = 8,
    STAGED = 9,
    UNSTAGED = 10,
    UNTRACKED = 11,
    NEW_STAGED = 12,
    PATH = 13,
    STASH_INDEX = 14


def get_color(color_type: Type) -> int:
    """
    Get a color by its type.

    :param color_type: The type of the color
    :return:           The color
    """
    return curses.color_pair(color_type)


def get_highlighted(color_type: Type) -> int:
    """
    Get the highlighted version of a color by its type.

    :param color_type: The type of the color
    :return:           The highlighted color
    """
    return curses.color_pair(color_type + ADD)


def convert_to_highlighted(color: int) -> int:
    """
    Convert a color to its highlighted version.

    **Note**: Passing in a highlighted color will
              result in an undefined color.

    :param color: The color to be converted
    :return:      The highlighted version of the color
    """
    return curses.color_pair((color >> 8) + ADD)


def init() -> None:
    """ Init the default color scheme. """
    curses.init_color(COLOR_GRAY, 450, 450, 450)
    curses.init_color(COLOR_GOLD, 475, 325, 75)
    curses.init_color(COLOR_PURPLE, 317, 424, 654)  # rgb 81, 108, 167
    curses.init_color(COLOR_ORANGE, 800, 500, 200)  # rgb 204, 128, 51
    curses.init_color(COLOR_RED, 933, 329, 392)
    curses.init_color(COLOR_GREEN, 200, 450, 200)
    curses.init_color(COLOR_BLUE, 255, 478, 772)  # rgb 65, 122, 197
    curses.init_color(COLOR_LIGHT_ORANGE, 972, 674, 349)
    curses.init_color(COLOR_LIGHT_BLUE, 737, 949, 964)

    bg: int = curses.COLOR_BLACK
    h_bg: int = COLOR_GOLD

    # Normal colors
    curses.init_pair(Type.NORMAL, curses.COLOR_WHITE, bg)
    curses.init_pair(Type.DIVIDER, COLOR_GRAY, bg)
    curses.init_pair(Type.HASH, COLOR_ORANGE, bg)
    curses.init_pair(Type.ADDED, COLOR_GREEN, bg)
    curses.init_pair(Type.REMOVED, COLOR_RED, bg)
    curses.init_pair(Type.DATE, COLOR_LIGHT_ORANGE, bg)
    curses.init_pair(Type.NAME, COLOR_LIGHT_BLUE, bg)
    curses.init_pair(Type.EMAIL, COLOR_PURPLE, bg)
    curses.init_pair(Type.STAGED, COLOR_PURPLE, bg)
    curses.init_pair(Type.UNSTAGED, COLOR_PURPLE, bg)
    curses.init_pair(Type.UNTRACKED, COLOR_RED, bg)
    curses.init_pair(Type.NEW_STAGED, COLOR_GREEN, bg)
    curses.init_pair(Type.PATH, COLOR_BLUE, bg)
    curses.init_pair(Type.STASH_INDEX, COLOR_PURPLE, bg)

    # Highlighted Colors
    curses.init_pair(Type.NORMAL + ADD, curses.COLOR_BLACK, h_bg)
    curses.init_pair(Type.DIVIDER + ADD, COLOR_GRAY, h_bg)
    curses.init_pair(Type.HASH + ADD, COLOR_ORANGE, h_bg)
    curses.init_pair(Type.ADDED + ADD, COLOR_GREEN, h_bg)
    curses.init_pair(Type.REMOVED + ADD, COLOR_RED, h_bg)
    curses.init_pair(Type.DATE + ADD, COLOR_LIGHT_ORANGE, h_bg)
    curses.init_pair(Type.NAME + ADD, COLOR_LIGHT_BLUE, h_bg)
    curses.init_pair(Type.EMAIL + ADD, COLOR_PURPLE, h_bg)
    curses.init_pair(Type.STAGED + ADD, COLOR_PURPLE, h_bg)
    curses.init_pair(Type.UNSTAGED + ADD, COLOR_PURPLE, h_bg)
    curses.init_pair(Type.UNTRACKED + ADD, COLOR_RED, h_bg)
    curses.init_pair(Type.NEW_STAGED + ADD, COLOR_GREEN, h_bg)
    curses.init_pair(Type.PATH + ADD, COLOR_BLUE, h_bg)
    curses.init_pair(Type.STASH_INDEX + ADD, COLOR_PURPLE, h_bg)
