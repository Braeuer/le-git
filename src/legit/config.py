"""
Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""
from configparser import ConfigParser, SectionProxy
from enum import Enum
from pathlib import Path

from legit.keys import ascii_to_key


class Section(Enum):
    """
    Enumeration of all existing sections in the config.
    """
    FILE_TREE_LAYOUT = 'file tree layout'
    BRANCH_LAYOUT = 'branch layout'
    STASH_LAYOUT = 'stash layout'
    GLOBAL_KEYS = 'global keys'
    REPOSITORY_SET = 'repository set'


class Field(Enum):
    """
    Enumeration of all existing fields config.
    """
    # Accessible through Section.FILE_TREE_LAYOUT
    ADD_MENU_KEY = 'add_menu_key'
    ADD_ALL_FILES = 'add_all_files'
    ADD_FILE = 'add_file'
    ADD_SELECTED_FILES = 'add_selected_files'
    DIFF_MODE_KEY = 'diff_mode_key'
    PREVIEW_MODE_KEY = 'preview_mode_key'
    LOG_MODE_KEY = 'log_mode_key'
    DEFAULT_MODE = 'default_mode'

    # Accessible through Section.GLOBAL_KEYS
    NEXT_LAYOUT = 'next_layout'
    PREV_LAYOUT = 'prev_layout'
    SWITCH_LAYOUT_1 = 'switch_layout_1'
    SWITCH_LAYOUT_2 = 'switch_layout_2'
    SCROLL_DOWN = 'scroll_down'
    SCROLL_UP = 'scroll_up'
    ENTER = 'enter'
    BACKSPACE = 'backspace'
    UP = 'up'
    DOWN = 'down'
    RIGHT = 'right'
    LEFT = 'left'
    CANCEL = 'cancel'
    NO = 'no'
    YES = 'yes'
    START_MENU = 'start_menu'
    TOGGLE_MARK = 'toggle_mark'

    # Accessible through Section.REPOSITORY_SET
    PUSH_MENU_KEY = 'push_menu_key'
    PUSH_KEY = 'push_key'
    FORCE_PUSH_KEY = 'force_push_key'
    PULL_MENU_KEY = 'pull_menu_key'
    PULL_KEY = 'pull_key'
    FORCE_PULL_KEY = 'force_pull_key'
    STATUS_KEY = 'status_key'
    COMMIT_KEY = 'commit_key'

    # Accessible through Section.FILE_TREE_LAYOUT, Section.STASH_LAYOUT,
    # Section.BRANCH_LAYOUT
    ICON = 'icon'

    # Accessible through Section.BRANCH_LAYOUT
    DELETE_BRANCH_MENU_KEY = 'delete_menu_key'
    CHECKOUT_NEW_KEY = 'checkout_new_key'
    SWITCH_KEY = 'switch_key'
    DELETE_BRANCH_KEY = 'delete_key'
    FORCE_DELETE_KEY = 'force_delete_key'
    RENAME_BRANCH_KEY = 'rename_key'

    # Accessible through Section.STASH_LAYOUT
    APPLY_MENU_KEY = 'apply_menu_key'
    DELETE_MENU_KEY = 'delete_menu_key'
    DELETE_KEY = 'delete_key'
    POP_KEY = 'pop_key'
    CLEAR_KEY = 'clear_key'
    APPLY_KEY = 'apply_key'
    CHECKOUT_KEY = 'checkout_key'


class Config:
    """
    A configuration parsing and handling values from the configuration file.

    The config file is loaded on creation.
    """

    def __init__(self, path: str):
        # Definitions
        self._config: ConfigParser
        self._file_icon_default: str
        self._directory_icon_default: str
        self._file_icon_section: SectionProxy
        self._directory_icon_section: SectionProxy

        # Initialization
        self._config = ConfigParser()
        self._config.read(path)
        self._file_icon_section = self._config['file icons']
        self._directory_icon_section = self._config['directory icons']
        self._file_icon_default = self._file_icon_section.get('default', ' ')
        self._directory_icon_default = ' '

    def get_string(self, section: Section, field: Field) -> str:
        """
        Get an arbitrary string from the config file.

        :param section: The name of the section
        :param field: The name of the field within the section
        :return: The retrieved string
        """
        return self._config[section.value].get(field.value)

    def get_key(self, section: Section, field: Field) -> int:
        """
        Get arbitrary key of the config file.

        :param section: The name of the section
        :param field: The name of the filed within the section
        :return: The key in the config
        """
        return ascii_to_key(
            self._config[section.value].get(field.value))

    def get_file_icon(self, file_ending: str) -> str:
        """
        Get the icon for a specific file ending.

        If there is no icon available for this file
        ending, the default icon/string is used.

        :param file_ending: The file ending to get the icon for
        :return: The available icon for this file ending
        """
        return self._file_icon_section.get(file_ending,
                                           self._file_icon_default)

    def get_directory_icon(self, expanded: bool) -> str:
        """
        Get the icon for closed/expanded directories.

        If there is no ico available for directories,
        the default icon/string is used.

        :param expanded: A flag whether the directory is expanded or not
        :return: The available icon for closed/expanded directories
        """
        if expanded:
            return self._directory_icon_section.get(
                'expanded',
                self._directory_icon_default
            )
        else:
            return self._directory_icon_section.get(
                'closed',
                self._directory_icon_default
            )

    def get_search_path(self) -> Path:
        """
        Get the path for starting he repository search.

        Default is the home directory.

        :return: The search path
        """
        return Path(self._config['global'].get('search_path', '~')
                    .replace('~', str(Path.home())))
