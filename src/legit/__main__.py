"""
Entry point for LeGit.

Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""
import curses
import os
from pathlib import Path

from legit import context, config, scheme
from legit.environment import Environment, PageType
from legit.ui.pages import base, start, main


class LeGit:
    """
    Main class and entry point for LeGit application.

    :ivar env: The environment of the application,
               handling the key cycle
    """

    def __init__(self, stdscr: curses.window):
        """
        Initialize LeGit and start it.

        :param stdscr: The root window of the application
        """
        curses.curs_set(0)
        scheme.init()
        conf: config.Config = config.Config('./config/config.ini')

        env: Environment = Environment(stdscr)
        ctx: context.Context = context.Context(stdscr, conf, env)

        path: Path = Path(conf.get_search_path())
        start_page: base.Page = start.Page(ctx, path)
        main_page: base.Page = main.Page(ctx)

        env.pages[PageType.START] = start_page
        env.pages[PageType.MAIN] = main_page

        self.env: Environment = env

    def run(self) -> None:
        """ Run LeGit. """
        self.env.selected = PageType.START
        self.env.start()


# Curses has a default on ESC by default.
# This sets down the delay.
# See: ESCDELAY in section Environment in https://linux.die.net/man/3/ncurses
os.environ['ESCDELAY'] = '5'
curses.wrapper(lambda stdscr: LeGit(stdscr).run())
