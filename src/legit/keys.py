"""
A module for converting ascii to key codes and vice versa.

Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""

import curses
from typing import Dict

ascii_keys: Dict[str, int] = {
    'ESC': 27,
    'TAB': ord('\t'),
    'ENTER': ord('\n'),
    'BACKSPACE': curses.KEY_BACKSPACE,
    'SPACE': ord(' '),
    'SHIFT+TAB': curses.KEY_BTAB,
    'UP': curses.KEY_UP,
    'DOWN': curses.KEY_DOWN,
    'LEFT': curses.KEY_LEFT,
    'RIGHT': curses.KEY_RIGHT,
    'F1': curses.KEY_F1,
    'F2': curses.KEY_F2,
    'F3': curses.KEY_F3,
    'F4': curses.KEY_F4,
    'F5': curses.KEY_F5,
    'F6': curses.KEY_F6,
    'F7': curses.KEY_F7,
    'F8': curses.KEY_F8,
    'HOME': curses.KEY_HOME,
    'END': curses.KEY_END,
    'PAGE_UP': curses.KEY_PPAGE,
    'PAGE_DOWN': curses.KEY_NPAGE
}

keys_ascii: Dict[int, str] = {y: x for x, y in ascii_keys.items()}


def key_to_ascii(key: int) -> str:
    """
    Convert a key code to the respective ascii name representation.

    :param key: The key code to be converted
    :return:    The ascii name representation of the key code
    """
    return keys_ascii.setdefault(key, chr(key))


def ascii_to_key(name: str) -> int:
    """
    Convert an ascii name the respective code.

    :param name: The name of the key
    :return: The converted key code
    """
    if len(name) == 1:
        return ascii_keys.setdefault(name, ord(name))
    else:
        return ascii_keys[name]
