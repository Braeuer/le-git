"""
Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""
import curses
from enum import Enum
from typing import Optional, Dict

from legit import handler
from legit.ui.pages import base
from legit.ui.popup import Popup


class PageType(Enum):
    """ Enumeration of names of different layout set types. """
    START = 0
    MAIN = 1


class Environment(handler.KeyCommandHandler):
    """
    Class for starting/stopping LeGit and handling the application cycle.
    """

    def __init__(self, stdscr: curses.window):
        """
        Init LeGit's environment.

        :param stdscr: The root window of the LeGit
        """
        handler.KeyCommandHandler.__init__(self)

        self._layouts: Dict[PageType, base.Page] = {}
        self._selected: Optional[base.Page] = None
        self._popup: Optional[Popup] = None
        self._stdscr: curses.window = stdscr
        self._running: bool = False

        self.register_key_command(curses.KEY_RESIZE, self.resize)

    @property
    def popup(self) -> Optional[Popup]:
        """
        Get the current popup if available.

        :return: The popup or None if not available
        """
        return self._popup

    @popup.setter
    def popup(self, p: Optional[Popup]) -> None:
        """
        Set/clear the current popup.

        :param p: The popup to be set
        """
        # Set the popup to None first to avoid recursive calls due to p.hide()
        tmp: Optional[Popup] = self._popup
        self._popup = None

        if tmp is not None:
            tmp.hide()

        self._popup = p

    @property
    def pages(self) -> Dict[PageType, base.Page]:
        """
        Get the layout sets dictionary

        :return: The layout sets dictionary
        """
        return self._layouts

    @property
    def selected(self) -> Optional[base.Page]:
        """
        Get the current layout set

        :return: The current layout set
        """
        return self._selected

    @selected.setter
    def selected(self, name: Optional[PageType]) -> None:
        """
        Set the current layout set by name.

        **Note**: This will set and apply the layout set
        """
        self._selected = self._layouts.get(name)
        if self._selected:
            self._selected.resize()
            self._selected.draw()

    def start(self) -> None:
        """
        Start the application to handle input keys.

        Keys are handled in the following order:
            1. Pass to popup, if there is one
            2. Handle the command in the environment
            3. Pass to active page

        If the key was handled in one step, any further
        step will be ignored.

        **Note**: If a popup is currently shown, the key is only
                  passed to the popup, even if the popup does not
                  handle the key!
        """
        self._running = True

        while self._running:
            key: int = self._stdscr.getch()

            self._stdscr.erase()

            if not self._popup:
                if not self.handle_command(key) and self._selected:
                    self._selected.handle_command(key)
            else:
                self._popup.handle_command(key)

            if self._selected:
                self._selected.draw()
            if self._popup:
                self._popup.draw()

            self._stdscr.refresh()

    def resize(self) -> None:
        """ Resize all shown views to the current terminal size. """
        if self._popup is not None:
            self._popup.scale_to_viewport()

        if self.selected is not None:
            self.selected.resize()

    def redraw(self) -> None:
        """
        Redraw the screen.

        This is meant to be called outside the normal key handling cycle,
        e.g. opening a popup is triggered by some external event.
        """
        self._stdscr.erase()

        self.selected.draw()
        if self.popup:
            self.popup.draw()

        self._stdscr.refresh()

    def stop(self) -> None:
        """ Stop LeGit. """
        self._running = False
