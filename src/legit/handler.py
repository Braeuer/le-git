"""
A module for different handler interfaces/base classes.

Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""
from abc import ABC
from collections import OrderedDict
from typing import Dict, Callable, Tuple, Optional

from legit import keys

key_command = Callable[[], None]


class KeyCommandHandler(ABC):
    """
    Handler for registering commands for specific keys.

    :ivar _commands: Holds all registered key commands
    """

    def __init__(self):
        self._commands: Dict[int, (key_command, str)] = OrderedDict()
        self._register_key_commands()

    def register_key_command(self, key: int, command: key_command,
                             summary: Optional[str] = None,
                             description: Optional[str] = None) -> None:
        """
        Register a command for a certain key.

        If there is already a command registered for the
        given key, the new command will be discarded.

        :param key:         The key to register the command for
        :param command:     The command to be registered
        :param summary:     A summary explaining the command
        :param description: A description explaining the command
        """
        self._commands[key] = (command, summary)

    def handle_command(self, key: int) -> bool:
        """
        Execute the command that is associated with a key if it exists.

        :param key: The key to execute the command for
        :return:    True if a command was executed, else False
        """
        item: Tuple[key_command, str] = self._commands.get(key, None)

        if item is not None:
            item[0]()
            return True

        return False

    def generate_summary(self) -> str:
        """
        Generate a multiline summary explaining each command.

        The summary is created in reversed registration order,
        which means the last registered command is explained
        first in the multiline summary.

        :return: The generated summary
        """
        text: str = ''
        info: Optional[str]
        key: str
        l_pad: int = max(len(keys.key_to_ascii(k)) for k, v in
                         self._commands.items() if v[1] is not None)

        for k, v in reversed(self._commands.items()):
            info = v[1]
            if info is not None:
                key = keys.key_to_ascii(k)

                text += f'{key.ljust(l_pad)} -> {info}\n'

        return text

    def _register_key_commands(self) -> None:
        """
        Register basic key commands that belong to this popup.

        **Note**: This function is called automatically in the
                  constructor.
        """
