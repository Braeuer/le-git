"""
LeGit's current version for single-sourcing.

Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""
__version__ = '0.0.1'
