"""
Module containing Git related functionality.

Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""
import pathlib
import signal
from enum import auto, Enum
from pathlib import Path
from typing import Optional, Callable, List

import git
import pexpect

OnCredentials = Callable[['Git', 'CredentialsType'], None]
OnComplete = Callable[['Git', int, str], None]
OnSpawn = Callable[['Git'], None]


class CmdError(Exception):
    """ Error thrown when a Git command failed (exit status != 0). """


class CredentialsType(Enum):
    """ Enum of different credentials prompt types. """
    USERNAME = auto(),
    PASSWORD = auto(),
    SSH_KEY = auto()


class Git:
    """
    A class for managing a git repository in a specific path.

    This class offers getting status data like the current branch,
    as well as executing arbitrary Git commands which may or may
    not require authentication. For commands that do not require
    authentication use `exec()`, for commands that may ask for
    authentication use `exec_remote()`.

    All Git commands that are related to an existing repository are
    executed in the context of the current repository with the option
    `-C <path-to-repository>`. There are other options are added to a
    Git command as well, in order to ensure wanted behavior. For a list
    of added options see `self._exec_cmd()`

    This class uses plain Git commands as well as GitPython at the moment.
    """

    def __init__(self, path: Path):
        """
        Init a Git repository.

        :param path: The local path of the Git repository
        """
        self._child: Optional[pexpect.spawn] = None
        self._cmd: str = ''
        self._ssh_tries: int = 0
        self._on_credentials: Optional[OnCredentials] = None
        self._on_complete: Optional[OnComplete] = None
        self._repo: git.Repo = git.Repo(str(path))

    @property
    def ssh_retry(self) -> bool:
        """
        Check whether the SSH key password prompt has already been shown.
        """
        return self._ssh_tries > 1

    @property
    def cmd(self) -> Optional[str]:
        """
        Get the currently/lastly executed command.

        The command string does **not** include the path to the
        current repository as flag `-C <path-to-repository>`.
        """
        return self._cmd

    @property
    def branch(self) -> str:
        """ Get the active branch name of the current repository. """
        return self._repo.active_branch

    @property
    def remote(self) -> str:
        """ Get the remote url of the current repository. """
        return self._repo.remote().url

    @property
    def path(self) -> pathlib.Path:
        """ Get the local path of the current repository """
        return pathlib.Path(self._repo.working_dir)

    @property
    def branches(self) -> List[str]:
        """ Get all local branches. """
        return list(self._repo.heads)

    def history(self, path: pathlib.Path, max_count: int) -> List[str]:
        """
        Get the last x commits of a file/directory.

        **Note**: This function is faster than using `self.exec()`,
                  because it avoids creating an extra process.

        :param path:      The path to the file/directory to get the diff from
        :param max_count: The maximum number of commits
        :return:          The last x commits as a list of lines
        """
        history: str = self._repo.git.log(str(path), max_count=max_count)
        return [] if not history else history.replace('\r', '').split('\n')

    def diff(self, path: pathlib.Path) -> List[str]:
        """
        Get the diff of a file/directory.

        **Note**: This function is faster than using `self.exec()`,
                  because it avoids creating an extra process.

        :param path: The path to the file/directory to get the diff from
        :return:     The diff as a list of lines
        """
        diff: Optional[str] = self._repo.git.diff(None, str(path))
        return [] if not diff else diff.replace('\r', '').split('\n')

    def exec(self, cmd: str, subcmd: Optional[str], options: List[str],
             *args) -> List[str]:
        """
        Execute a local Git command with options.

        CRLF is replaced by LF.

        :param cmd:     The Git command to be executed
        :param subcmd:  The Git subcommand to be executed
        :param options: Git options to be added to `git push`
        :param args:    Arguments like <branch> or <stash-index> as str
        :return:        The git output as a list of lines
        :raise:         CmdError When Git exits with status != 0
        """
        self._exec_cmd(cmd, subcmd, options, *args)

        if self._child is None:
            return []

        res: str = self._child.read().replace('\r', '').rstrip()

        if self._child.wait() != 0:
            raise CmdError(res)

        return res.split('\n') if res else []

    def exec_remote(self, cmd: str, subcmd: Optional[str],
                    on_credentials: OnCredentials, on_spawn: OnSpawn,
                    on_complete: OnComplete, options: List[str],
                    *args) -> None:
        """
        Execute a Git command that might need credentials.

        :param cmd:            The Git command to be executed
        :param subcmd:         The Git subcommand to be executed
        :param on_credentials: Callback when waiting for credentials
        :param on_spawn:       Callback when process has been spawned, but
                               before waiting for credentials
        :param on_complete:    Callback when push completed (success or error)
        :param options:        Git options to be added to `git push`
        :param args:           Arguments like <branch> or <stash-index> as str
        """
        self._ssh_tries = 0
        self._exec_cmd(cmd, subcmd, options, *args)
        on_spawn(self)
        self._on_complete = on_complete
        self._on_credentials = on_credentials
        self._wait_for_credentials()

    def cancel(self) -> None:
        """
        Cancel the current child process by sending ^C.

        **Note**: Depending on the status of the child process, cancelling
                  might do nothing and changes made by the child process are
                  permanent.
        **Note**: Cancelling a child process might have unwanted effects.
        """
        if self._child:
            self._child.kill(signal.CTRL_C_EVENT)

    def send(self, text: str) -> None:
        """
        Send text to the current child process.

        **Note**: This function waits for credentials prompt/completion of
                  the child process. If there is no child process, nothing
                  will be done.

        :param text: The text to send to the child process
        """
        if self._child:
            self._child.sendline(text)
            self._wait_for_credentials()

    def _wait_for_credentials(self) -> None:
        """
        Wait for credentials prompt/completion of the child process.

        **Note**: This function waits for the first event
                  (credentials prompt/completion) to occur,
                  even if no callback exists.
        """
        if self._child is not None:
            index: int = self._child.expect(
                ['Enter passphrase for key.*:',
                 "Username for 'https://.*:",
                 "Password for 'https://.*:",
                 pexpect.EOF])

            if index == 0:
                if self._on_credentials is not None:
                    self._ssh_tries += 1
                    self._on_credentials(self, CredentialsType.SSH_KEY)
            elif index == 1:
                if self._on_credentials is not None:
                    self._on_credentials(self, CredentialsType.USERNAME)
            elif index == 2:
                if self._on_credentials is not None:
                    self._on_credentials(self, CredentialsType.PASSWORD)
            elif (index == 3 and self._child is not None
                  and self._on_complete is not None):
                self._child.wait()
                self._on_complete(self, self._child.exitstatus,
                                  self._child.before)

    def _exec_cmd(self, cmd: str, subcmd: Optional[str],
                  options: List[str], *args) -> None:
        """
        Execute a Git command with extra global options.

        The following options are added:
            1.  -c color.ui=false to prevent ANSI escape sequence in
                the output (woks only in combination with 2.)
            2.  --no-pager to prevent paging and get the whole output
                from Git immediately.
            3.  -C <repo-path> to execute Git within <repo-path>
        """
        sub: List[str] = [] if subcmd is None else [subcmd]
        # Set executing path via `-C`, output to be uncolored
        # with `-c' and print everything at ones with `--no-pager`
        arguments: List[str] = ['-c', 'color.ui=false', '--no-pager', '-C',
                                str(self.path), cmd, *sub, *options, *args]
        self._cmd = f'git {" ".join(arguments[5:])}'.strip()
        self._child = pexpect.spawn('git', arguments, encoding='utf-8')
