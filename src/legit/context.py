"""
Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""
import curses
from dataclasses import dataclass
from typing import Optional, Any

import legit
from legit.git import Git


@dataclass
class Context:
    """
    The application context, holding all globally accessible data.

    :ivar root_window: The root window of the application (size of the
                       terminal)
    :ivar config:      The configuration of the application
    :ivar env:         The environment handling windows and key inputs
    :ivar git:         The currently used Git object (e.g. holds the
                       current repository)
    """
    root_window: curses.window
    config: legit.config.Config
    env: Any
    git: Optional[Git] = None
