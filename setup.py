"""
Author: Matthias Bräuer <matthias@braeuer.dev>
License: MIT
"""

import pathlib
from typing import Dict

from setuptools import find_packages, setup


def get_version() -> str:
    """
    Read the current version from the version module.

    :return: The current version
    """
    version: Dict = {}
    exec(pathlib.Path('src/legit/_version.py').read_text(), version)
    return version['__version__']


VERSION: str = get_version()

#: The project repository
PROJECT_URL: str = 'https://gitlab.com/Braeuer/le-git'
#: The current release archive
RELEASE_URL: str = f'{PROJECT_URL}/-/archive/{VERSION}/legit-{VERSION}.tar.gz'

setup(
    name='LeGit',
    version=VERSION,
    author='Matthias Bräuer',
    license='MIT',

    description='A simple Git TUI for improving and learning Git',
    long_description=pathlib.Path('README.md').read_text(),
    long_description_content_type='text/markdown',
    keywords=['git', 'version control'],

    url=PROJECT_URL,
    download_url=RELEASE_URL,

    packages=find_packages('src'),
    package_dir={'': 'src'},
    install_requires=['GitPython', 'pexpect'],
    entry_points={'console_scripts': ['legit=legit.__main__.py:main']},
    python_requires='>=3.6',

    classifiers=[
        'Environment :: Console',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
    ]
)
